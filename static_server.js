var express = require('express')
var app = express()
var compression = require('compression')

// Define the port to run on
app.set('port', process.env.PORT || 3000)
app.use(compression())

app.get('*.js', function(req, res, next) {
	req.url = req.url + '.gz'
	res.set('Content-Encoding', 'gzip')
	next()
})

app.use('/', express.static('./dist', { maxage: '1d' }))
app.use('/public', express.static('./public', { maxage: '1d' }))

app.use((req, res) => {
	res.sendFile(`${__dirname}/dist/index.html`)
})

// Listen for requests
var server = app.listen(app.get('port'), function() {
	var port = server.address().port
	console.log(`Environment: ${process.env.NODE_ENV}`)
	console.log(`Mode: ${process.env.MODE}`)
	console.log('Magic happens on port ' + port)
})
