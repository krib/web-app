const express = require('express')
const app = express()
const fs = require('fs')
const expressNunjucks = require('express-nunjucks');
const path = require('path')
const isDev = true;

// Routes
const Listings = require('./routes/listings')
const Listing = require('./routes/listing')
const Pages = require('./routes/pages')

// Static Pages
app.get('/public/areas/:page', function (req, res, next) {
	if (!fs.existsSync('./public/areas/' + req.params.page)) {
		return res.send('')
	}
	next()
})

// Static
app.use('/public', express.static('./public'))
app.use('/dist', express.static('./dist'));
app.set('views', path.join(__dirname, 'views'));
app.set('port', process.env.PORT || 8080);

// Templates
const njk = expressNunjucks(app, {
	watch: isDev,
	noCache: false
});


app.get('/', Listings)
app.get('/listings/:filter/:type', Listings)
app.get('/listing/:listingId', Listing)
app.get('/pages/:page', Pages)
app.get('*', Listings)


app.listen(app.get('port'), function () {
	console.log('Example app listening on port ' + app.get('port'))
})
