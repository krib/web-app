const fs = require('fs')
import config from './../../public/pages/config.js'

module.exports = function (req, res, next) {
  if (!config[req.params.page]) {
    return next()
  }
  fs.readFile('public/pages/'+req.params.page+'.html', 'utf8', function (err,data) {
    res.render('pages', {
      content: data,
      title: config[req.params.page].title,
      description: config[req.params.page].description,
      keywords: config[req.params.page].keywords,
    })
  });

}
