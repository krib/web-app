import ListingsService from './../../app/services/ListingsService'

const header = {
  title: 'Property Listings in Singapore',
  description: 'Some description you can update later.',
  keywords: 'kw1, kw2'
}

module.exports = function (req, res) {
  if (req.params.filter) {
    ListingsService()
      .getListings(req.params.filter, req.params.type, null, 40)
      .then(listings => {
        res.render('listings', {listings: listings, title: header.title, description: header.description, keywords: header.keywords});
      })
    return
  }

  ListingsService()
    .getListings(null, null, null, 40)
    .then(listings => {
      res.render('listings', {listings: listings, title: header.title, description: header.description, keywords: header.keywords});
    })
}
