import ListingsService from './../../app/services/ListingsService'

module.exports = function (req, res) {
  ListingsService()
    .getListing(req.params.listingId)
    .then(listing => {
      const header = {
        title: listing.address.streetName + ' ' + listing.address.postalCode,
        description: 'Some description you can update later.',
        keywords: 'kw1, kw2'
      }
      res.render('listing', {listing: listing, title: header.title, description: header.description, keywords: header.keywords});
    })
}
