const config = require('./config')
const firebase = require('firebase')

if (firebase.apps.length === 0) {
    firebase.initializeApp(config)
}

module.exports =  firebase
