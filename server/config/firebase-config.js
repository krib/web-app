let config = {}

if (process.env.MODE === 'production') {
  // FOR PRODUCTION
  config = {
    apiKey: "AIzaSyCp_v9ceo0ar0hBt2ctF874Sla-Mv998lU",
    authDomain: "krib-production.firebaseapp.com",
    databaseURL: "https://krib-production.firebaseio.com",
    projectId: "krib-production",
    storageBucket: "krib-production.appspot.com",
    messagingSenderId: "198649457696"
  }
} else {
  // FOR STAGING
  config = {
    apiKey: "AIzaSyCWOcZBjAvgQ_ch6rHcj8SD_jZKILfWf70",
    authDomain: "krib-staging.firebaseapp.com",
    databaseURL: "https://krib-staging.firebaseio.com",
    projectId: "krib-staging",
    storageBucket: "krib-staging.appspot.com",
    messagingSenderId: "568366293812"
  };
}

console.log(`Configured for ${config.projectId}`)

module.exports = config
