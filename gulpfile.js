var gulp        = require('gulp');
var sass        = require('gulp-sass');
var inject      = require('gulp-inject');
var concat      = require('gulp-concat');

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'inject'], function() {
    gulp.watch("scss/*.scss", ['sass', 'inject']);
});

gulp.task('watch', ['sass:concat'], function() {
    gulp.watch("scss/*.scss", ['sass:concat']);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("scss/*.scss")
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest("public/css"))
});

gulp.task('inject', function () {
  var target = gulp.src('./views/layout/layout.html');
  var sources = gulp.src(['./public/css/*.css'], {read: false});
  return target.pipe(inject(sources))
    .pipe(gulp.dest('./views/layout'));
});

gulp.task('sass:concat', function() {
    return gulp.src("scss/*.scss")
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest("public/css"))
});

gulp.task('default', ['serve']);
