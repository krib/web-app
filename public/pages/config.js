// Configuration for pages
const config = {
	// About page
	about: {
		title: 'About Krib',
		description: 'Krib is an innovative tech startup that aims to revolutionise the way tenants find the perfect home in an overwhelming property market.',
		keywords: 'Krib tenant landlord agent real-estate broker',
	},
	contact: {
		title: 'Contact',
		description: 'Our customer support team is committed to making renting easy for you.',
		keywords: 'Contact Krib',
	},
	privacy: {
		title: 'Privacy',
		description: 'Privacy policy of Krib Online Pte Ltd.',
		keywords: 'Krib Privacy Policy',
	},
	faq: {
		title: 'Frequently Asked Questions',
		description: 'Frequently Asked Questions',
		keywords: 'Krib faq questions',
	},
	terms: {
		title: 'Terms of Use',
		description: 'Terms of Use',
		keywords: 'Krib terms conditions use',
	}
}

export default config
