# Krib.co Website / App

This is readme file for Krib.co website / app.

## Getting Started

1. Run `npm install`
2. Run `npm run dev` (to enable dev Server, will be running on localhost:3000)
3. In another terminal tab run `gulp watch` if you plan to change css files (stored in `scss` folder)
4. To deploy check deployments section

## App Structure

App is separated in two main folders:

1. `/app/` folder contains front-end (React) source code
2. `/server/` folder contains server for server-side rendering
3. `/public/` folder contains static assets for the app


### Frontend app folder (`/app/`)

1. Actions, Constants, Reducers, Store -- Redux folders
2. Components -- only shared components (used in multiple routes)
3. Layouts -- Layout components for different app views
4. Routes -- Contains all routes for the app and composed views of components

Entry point is `main.js`.

# App Configuration

Configuration files:
- `/app/app.config.js` contains basic configuration variables for the app (banners, copy, etc)
- `/app/firebase.config.js` contains configuration for firebase (both staging and production)
- `/app/services/StaticModelsService.js` contains configuration for static (pre-defined models) such as *available facilities* and so on

## Updating Assets

1. To control images and copy of popular listings, go to `/app/routes/Home/components/popularAreas`
2. To control images of areas in popular listings, add images to `/public/images/areas`
3. To control icons for facilities create icons in `/public/icons/facilities`

## Deployments

To deploy app to `staging` or `production` you first need to add heroku remotes.

- Staging must be named `staging` (`git push staging master`)
- Production must be named `heroku` (`git push heroku master`)

In phase 1 generate production code by running:
- `npm run build:production`
- `npm run build:staging`

To deploy the app you need to `git commit` all changes, because only commited (`git add . `, `git commit -m "..."`) changes will be deployed to server.
Then use one of the following commands to deploy:

- `git push heroku master`
- `git push staging master`
