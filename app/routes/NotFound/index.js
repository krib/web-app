import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import AppLayout from './../../layouts/AppLayout'

// Set Timeout Here
const TIMEOUT = 10000

export default class extends Component {
  state = {
    redirect: false
  }

  componentDidMount() {
    setTimeout(function() {
      this.setState({ redirect: true })
    }.bind(this), TIMEOUT)
  }

  render() {
    if (this.state.redirect) return <Redirect to="/" />
    return (
      <AppLayout>
        <h4>Oops!</h4>
        <p>We can't seem to find the page you're looking for. You will be redirected to our <a href="/">home page</a> in a few seconds.</p>
      </AppLayout>
    )
  }
}
