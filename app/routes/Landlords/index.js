import React from 'react'
import AppLayout from './../../layouts/AppLayout'
import {Link} from 'react-router-dom'

import ListingsService from './../../services/ListingsService'

export default class Landlords extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      landlords: []
    }
  }

  componentDidMount() {
    ListingsService()
      .getLandlords()
      .then(landlords => this.setState({landlords}))
      .then(res => console.log(this.state))
  }

  render = () => {
    return (
      <AppLayout>
        {
          this.state.landlords.map((landlord) => (
            <div key={landlord.listedBy}>
              Landlord name: {landlord.landlord.displayName} <br/>
              Photo: <img src={landlord.landlord.photoUrl} />
              Total Listings: {landlord.listings.length}<br/>
              <Link to={`/listings/landlord/${landlord.listedBy}`} className="btn btn-primary">View Listings</Link>
              <hr/>
            </div>
          ))
        }

      </AppLayout>
    )
  }
}
