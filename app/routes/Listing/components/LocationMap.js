import React from 'react'
import { withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps";

export default withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={props.zoom || 17}
    defaultCenter={{ lat: props.marker.position.lat, lng: props.marker.position.lng }}
  >
    {
      !props.hideMarker &&
      <div>
        <InfoWindow
            {...props.marker}
             >
               {props.content}
        </InfoWindow>
        <Marker {...props.marker}></Marker>
      </div>
    }

  </GoogleMap>
));
