import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import store from 'store'
import MessagingService from './../../../services/MessagingService'
import ConversationService from './../../../services/ConversationService'
import {showLoginForm} from './../../../reducers/loginReducer'


class Landlord extends React.Component {
  state = {
    messageInput: '',
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    if (!this.props.user.id) {
      this.props.showLoginForm()
      return
    }
    store.remove(this.props.listing.key)
    let data = {
      userId: this.props.user.id,
      listedBy: this.props.listing.listedBy,
      message: this.state.messageInput,
      listingId: this.props.listing.key
    }
    this.setState({isSending: true})
    // GET ID
    ConversationService(data.listedBy, this.props.user.auth.ie)
      .then(res => {
        MessagingService()
          .createConversation(data, res.data.conversationId)
          .then(res => this.setState({redirect: true, conversationId: res}))
      })
  }

  handleChange = (evt) => {
    this.setState({messageInput: evt.target.value})
  }

  render() {
    if (this.state.redirect && !this.props.user.id) return <Redirect to={`/signin?source=/listing/${this.props.listing.key}`} />
    if (this.state.redirect) return <Redirect to={`/messages/${this.state.conversationId}`} />
    if (this.state.redirectToEdit) return <Redirect to={`/edit-listing/${this.props.listing.key}`} />
    return (
      <div>
        <div className="listing-landlord-avatar" style={{backgroundImage: `url(${this.props.listing.landlord.photoUrl || '/public/images/avatar_placeholder.png'})`}}></div>
        <h5>{this.props.listing.landlord.displayName}</h5>
        {
          this.props.listing.listedBy === this.props.user.id &&
           <p className="text-muted">This is your listing</p>
        }

        <div className="spacer-lg hidden-lg-up" ></div>
        {
          this.props.listing.listedBy !== this.props.user.id
          ? <button className="btn btn-sm btn-primary" onClick={() => this.setState({isContact: true})}>Send Message</button>
          : <button className="btn btn-sm btn-primary" onClick={() => this.setState({redirectToEdit: true})}>Edit Listing</button>
        }

        {
          this.state.isContact &&
          <form onSubmit={this.onSubmit}>
            <div className="spacer"></div>
            <div className="form-group">

              <label>Send a Message to {this.props.listing.landlord.displayName}</label>
              <textarea value={this.state.messageInput} onChange={this.handleChange} className="form-control" rows="5" required></textarea>
            </div>
              {
                this.state.isSending
                ? <button className="btn btn-primary btn-sm" disabled><i className="fa fa-fw fa-spinner fa-spin"></i> Sending...</button>
                : <input type="submit" value="Send Message" className="btn btn-primary btn-sm" />
              }

              <button className="btn btn-link btn-sm" onClick={() => this.setState({isContact: false})}>Cancel</button>
          </form>
        }
      </div>

    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => ({
  showLoginForm: () => dispatch(showLoginForm())
})
export default connect(mapStateToProps, mapDispatchToProps)(Landlord)
