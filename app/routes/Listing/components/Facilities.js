import React from 'react'

export default ({ facilities }) => (
	<div>
		<br></br>
		<h4>Available Facilities</h4>
		<br></br>
		<div className="row text-center" style={{ marginTop: '1rem' }}>
			{
				facilities.map((item, idx) => (
					<div className="col-6 col-md-4 col-lg-3" key={idx}>
						<img src={`/public/icons/facilities/${item.replace(/\s/g, '')}.svg`} alt="" className="listing-icon-facility" />
						 <div className="listing-icon-facility-text">{item}</div>
					</div>
				))
			}

		</div>
		{
			!facilities.length &&
			<span >No info about facilities.</span>
		}
	</div>
)
