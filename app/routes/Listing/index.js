import React from 'react'
import { Link } from 'react-router-dom'
import AppLayout from './../../layouts/AppLayout'
import { connect } from 'react-redux'

import ListingsService from './../../services/ListingsService'
import ListingItem from './../../components/ListingItem'
import PleaseWait from './../../components/PleaseWait'

import Landlord from './components/Landlord'
import LocationMap from './components/LocationMap'
import Facilities from './components/Facilities'

class Listing extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			listing: {
				address: {},
				photos: [],
				leaseType: {},
				leaseLengthPref: {},
				housingType: {},
				facilities: [],
				landlord: {},
			},
			isFetching: true,
		}
	}

	componentDidMount() {
		window.scrollTo(0,0)
		ListingsService()
			.getListing(this.props.match.params.id)
			.then(res => {
				let fac = []
				if (res.facilities) {
					for (let key in res.facilities) {
						if (res.facilities[key]) fac.push(key)
					}
				}
				res.facilities = fac
				return res
			})
			.then(listing => this.setState({ listing, activePhoto: (listing.photos.length) ? listing.photos[0].thumb_url : null }))
			.then(listing => {
				this.setState({ isFetching: false })
				console.log(this.state.listing)
			})
	}

	render() {
		if (this.state.isFetching) return null
		let bubbleContent = <div>
			<span style={{ color: "#333", marginBottom: '10px' }}>{this.state.listing.address.streetName}</span> <br />
			<span >
				Singapore, {this.state.listing.address.postalCode}
			</span>
		</div>
		let marker = {}
		let zoom = 17
		let hideMarker = false;
		if (this.state.listing.address.geoFireLocation) {
			marker = {
				position: {
					lat: this.state.listing.address.geoFireLocation.l[0],
					lng: this.state.listing.address.geoFireLocation.l[1],
				}
			}
		} else {
			marker = {
				position: {
					lat: 1.3733182,
					lng: 103.8150667,
				}
			}
			hideMarker = true
			zoom = 10
		}


		return (
			<AppLayout>
				<div className="row">
					<div className="col-md-6">
						<h3>
							<span className="text-secondary">
								${(Math.round(this.state.listing.rent / 100)).toLocaleString()} {' '}
							</span>
							{this.state.listing.leaseType.name} {this.state.listing.area ? ' in ' + this.state.listing.area : ''}
						</h3>
						<span className="lead">{this.state.listing.address.buildingName}</span> <br />
						<span className="lead text-muted">{this.state.listing.housingType.name}</span>
						{' '}
						{
							this.state.listing.leaseType.key === 2 &&
							<span className="text-muted">
								(
                <i className="fa fa-fw fa-bed"></i> {this.state.listing.bedrooms} {' '}
								<i className="fa fa-fw fa-bath"></i> {this.state.listing.bathrooms}
								)
              </span>
						}
						<br />
						<span className="lead text-muted">{this.state.listing.leaseLengthPref.name}</span>
						<hr className="hr-md" />
						<div className="row">
							<div className="col-sm-6">
								<img src={"/public/icons/housing-types/" + this.state.listing.housingType.alias + ".svg"} className="listing-icon-lg" />
								<div className="text-secondary text-bold" style={{ transform: 'translateY(10px)' }}>{this.state.listing.address.buildingName || 'No Building Name'}</div>
							</div>
							<div className="col-sm-6" >
								<img src={"/public/icons/length-prefs/" + this.state.listing.leaseLengthPref.alias + ".svg"} className="listing-icon-lg" />
								<div className="text-secondary text-bold" style={{ transform: 'translateY(10px)' }}>{this.state.listing.leaseLengthPref.name}</div>
							</div>
						</div>
						<br />
						<hr className="hr-md" />
						<Facilities facilities={this.state.listing.facilities} />
						<hr className="hr-md" />
						<Landlord listing={this.state.listing} />
						<hr className="hr-md" />
						<h4>Map</h4>
						<LocationMap
							containerElement={<div style={{ height: `300px` }} />}
							mapElement={<div style={{ height: `300px` }} />}
							marker={marker}
							content={bubbleContent}
							hideMarker={hideMarker}
							zoom={zoom}
						/>
					</div>
					<div className="col-md-6">
						{this.state.activePhoto &&
							<img src={this.state.activePhoto} className="btn-block listing-image-lg" />
						}
						<div className="d-flex flex-wrap">
							{
								this.state.listing.photos.map(photo => (
									<div
										onClick={() => this.setState({ activePhoto: photo.thumb_url })}
										key={photo.key}
										className={this.state.activePhoto === photo.thumb_url ? "listing-thumbnail active" : "listing-thumbnail"}
										style={{ backgroundImage: `url(${photo.thumb_url})` }}
									></div>
								))
							}
						</div>
					</div>
				</div>
			</AppLayout>
		)
	}
}


export default Listing
