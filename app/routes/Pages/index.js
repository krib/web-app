import React from 'react'
import AppLayout from './../../layouts/AppLayout'
import RenderHtml from './../../components/RenderHtml'

export default class extends React.Component {

  render() {
    return (
      <AppLayout>
        <RenderHtml url={`/pages/${this.props.match.params.slug}`} />
      </AppLayout>
    )
  }
}
