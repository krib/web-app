import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch, Redirect, withRouter } from 'react-router-dom'
import {connect} from 'react-redux'
import queryString from 'query-string'

// Routes
import Home from './Home'
import Listing from './Listing'
import Listings from './Listings'
import Landlords from './Landlords'
import Messages from './Messages'
import ManageListing from './ManageListing'
import Signin from './Signin'
import Profile from './Profile'
import Settings from './Settings'
import Pages from './Pages'
import CuratedListings from './CuratedListings'
import MyListings from './MyListings'
import NotFound from './NotFound'

import Auth from '../components/Auth'
import PleaseWait from '../components/PleaseWait'


class Routes extends React.Component {

  render () {
    return (
      <div>
        <Route exact path="*" component={Auth}/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/not-found" component={NotFound}/>
          <Route exact path="/landlords" component={Landlords}/>
          <Route exact path="/listing/:id" component={Listing}/>
          <Route exact path="/listings/:filter/:id" component={Listings}/>
          <Route exact path="/signin" component={Signin}/>
          <Route exact path="/new-listing" component={ManageListing}/>
          <Route exact path="/settings" component={Settings}/>
          <Route exact path="/pages/:slug" component={Pages}/>
          {!this.props.user.isFetched && <Route path="*" component={PleaseWait}/>}
          {!this.props.user.registrationToken && <Redirect to={'/not-found'} />}
          {!this.props.user.displayName && <Route path="*" component={Profile}/>}
          <Route exact path="/curated-listings" component={CuratedListings}/>
          <Route exact path="/my-listings" component={MyListings}/>
          <Route exact path="/profile" component={Profile}/>
          <Route exact path="/messages" component={Messages}/>
          <Route exact path="/messages/:id" component={Messages}/>
          <Route exact path="/edit-listing/:id" component={ManageListing}/>
          <Route exact path="*" component={NotFound} />
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user
})

export default withRouter(connect(mapStateToProps)(Routes))
