import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import store from 'store'
import {showLoginForm} from './../../reducers/loginReducer'

// Services
import CurateService from './../../services/CurateService'

// Components
import AppLayout from './../../layouts/AppLayout'
import { housingTypes, leaseTypes, leaseLengths, areas, otherPreferences } from './../../services/StaticModelsService'
import { areas as allAreas } from './../../services/AreasService'
let breadcrumbs = [{ name: 'Search Settings' }]


class Settings extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isActive: false,
			leaseTypePref: {},
			housingTypePref: {},
			leaseleaseLengthPref: {},
			budget: 0,
			areaPrefs: {},
			otherPrefs: {},
			pointOfInterest: {},
			_INPUT: '',
			isActive: true,
		}
	}

	componentDidMount() {
		// Get Request
		// We need timeout here because this route is not dependent on user
		// Even when logged out you can access this route
		setTimeout(() => this.fetchUserRequest(), 500)
	}

	fetchUserRequest() {
		CurateService()
			.getCurationRequest(this.props.user.id)
			.then(res => {
				if (!res) return
				this.setState(res)
				let hasTempCurateRequest = store.get('tempCurateRequest')
				if (hasTempCurateRequest) this.updateCurationPreferences(hasTempCurateRequest)
			})
	}

	updateCurationPreferences(data) {
		console.log(data)
		CurateService()
			.updateCurationRequest(this.props.user.id, data)
			.then(res => {
				store.remove('tempCurateRequest')
				this.fetchUserRequest()
				this.setState({ isSaved: true })
			})
	}

	onSubmit = () => {
		// If user is not logged in
		if (!this.props.user.id) {
			this.props.showLoginForm()
			return
		}
		CurateService()
			.updateCurationRequest(this.props.user.id, this.state)
			.then(res => {
				this.setState({ redirectToCurated: true })
			})
	}

	toggleCuration = () => {
		this.setState({ isActive: !this.state.isActive })
		// setState is async
		setTimeout(() => {
			CurateService()
				.updateCurationRequest(this.props.user.id, this.state)
		}, 100)
	}

	handleChange = (obj) => (event) => {
		let upd = {}
		upd[obj] = this.state[obj]
		upd[obj][event.target.name] = (event.target.type === 'checkbox') ? event.target.checked : event.target.value
		this.setState(upd)
		console.log(this.state)
	}

	handleInput = (key) => (event) => {
		let upd = {}
		upd[event.target.name] = event.target.value
		this.setState(upd)
	}

	handleListClick = (target, key) => {
		console.log(target, key)
		let obj = this.state[target]
		obj[key] = !obj[key]
		console.log(obj)
		let upd = {}
		upd = { ...this.state }
		upd[target] = obj
		this.setState(upd)
		console.log(this.state)
	}

	toggleArea = (area) => (evt) => {
		console.log(area)
		let upd = this.state;
		upd.areaPrefs[area] = !this.state.areaPrefs[area]
		this.setState(upd)
	}

	toggleOtherPrefs = item => evt => {
		let upd = this.state.otherPrefs
		upd[item.name] = !this.state.otherPrefs[item.name]
		this.setState({ otherPrefs: upd })
	}

	render() {
		if (this.state.redirect) return <Redirect to="/signin?source=/settings" />
		if (this.state.redirectToCurated) return <Redirect to="/curated-listings" />
		return (
			<AppLayout>
				<h2>Find Your Dream Home</h2>
				<p>Without wasting your time browsing thousands of property listings, let Krib do the work for you! Just tell us what your ideal home is, and we will curate a daily list of perfect homes for you.</p>
				<hr className="hr-lg" />
				<h4><span className="badge badge-info"><div style={{ transform: 'translateX(2px)' }}>1&nbsp;</div></span> Rent & Property Info</h4>
				<p>What is your budget, and what is your preferred housing type?</p>
				<div className="row">
					<div className="col-sm-3">
						<label>Rent <span className="text-muted">(in S$)</span></label>
						<input type="number" step="50" className="form-control" name="budget" value={this.state.budget} onChange={this.handleInput('budget')} required />
					</div>
					<div className="col-sm-3">
						<label>Lease Type</label>
						<ul className="list-group">
							{leaseTypes.map((item, idx) =>
								<li
									key={item.key}
									className={this.state.leaseTypePref[item.alias] ? "list-group-item active" : "list-group-item"}
									onClick={() => this.handleListClick('leaseTypePref', item.alias)}>{item.name}</li>
							)}
						</ul>
					</div>
					<div className="col-sm-3">
						<label>Housing Type</label>
						<ul className="list-group">
							{housingTypes.map((item, idx) =>
								<li
									key={item.key}
									className={this.state.housingTypePref[item.alias] ? "list-group-item active" : "list-group-item"}
									onClick={() => this.handleListClick('housingTypePref', item.alias)}>{item.name}</li>
							)}
						</ul>
					</div>
					<div className="col-sm-3">
						<label>Lease Length</label>
						<ul className="list-group">
							{leaseLengths.map((item, idx) =>
								<li
									key={item.key}
									className={this.state.leaseleaseLengthPref[item.alias] ? "list-group-item active" : "list-group-item"}
									onClick={() => this.handleListClick('leaseleaseLengthPref', item.alias)}>{item.name}</li>
							)}
						</ul>
					</div>
				</div>
				<hr className="hr-lg" />
				<h4><span className="badge badge-info">2</span>Preferred Locations</h4>
				<p>What are your preferred locations?</p>
				<p>
					Selected Areas: <br />
					{
						allAreas.map((area, idx) => (
							<span key={idx} onClick={this.toggleArea(area)}>
								{
									this.state.areaPrefs[area]
										? <button className="btn btn-sm btn-primary btn-toggle">
											{area}
											<i className="fa fa-fw fa-close"></i>
										</button>
										: null
								}
							</span>

						))
					}
				</p>
				<div className="form-group">
					<label>Start typing to filter areas or tap to select:</label>
					<input type="text" className="form-control" value={this.state._INPUT} onChange={(evt) => this.setState({ _INPUT: evt.target.value })} />
				</div>
				{/* Areas */}
				{
					allAreas.map((area, idx) => {
						if (area.indexOf(this.state._INPUT.slice(1)) !== -1)
							return (
								<span key={idx}>
									{
										this.state.areaPrefs[area]
											? <button className="btn btn-sm btn-primary btn-toggle" onClick={this.toggleArea(area)}>{area} <i className="fa fa-fw fa-close"></i></button>
											: <button className="btn btn-sm btn-outline-primary btn-toggle" onClick={this.toggleArea(area)}>{area}</button>
									}
								</span>

							)
					})
				}
				<hr className="hr-lg" />
				<br />
				<h4><span className="badge badge-info">3</span> Other Preferences</h4>
				<p>Indicate below if any of the following are important to you.</p>
				{
					otherPreferences.map(item => (
						<button
							key={item.key}
							className={this.state.otherPrefs[item.name] ? 'btn btn-sm btn-primary btn-toggle' : 'btn btn-sm btn-outline-primary btn-toggle'}
							onClick={this.toggleOtherPrefs(item)}
						>
							{item.name}
						</button>
					))
				}

				{
					this.props.user.id &&
					<div>
						<hr className="hr-lg" />
						<p>When you've found your ideal home, you can disable PropertyMatch from here.</p>

						<br />
						<div style={{ display: 'inline-block', verticalAlign: 'middle', marginRight: '10px' }}>
							<input id="cb2" className="tgl tgl-ios" type="checkbox" checked={this.state.isActive} onChange={this.toggleCuration} />
							<label htmlFor="cb2" className="tgl-btn"></label>
						</div>
						<div style={{ display: 'inline-block', transform: 'translateY(-3px)' }}>
							<strong>Enable PropertyMatch</strong>
						</div>


					</div>
				}
				<hr className="hr-lg" />
				<button className="btn btn-primary btn-lg pull-right" onClick={this.onSubmit}>Save</button>
				<div className="clearfix"></div>
			</AppLayout>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user
})

const mapDispatchToProps = dispatch => ({
	showLoginForm: () => dispatch(showLoginForm())
})

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
