import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import {connect} from 'react-redux'
import queryString from 'query-string'
import store from 'store'
// Components
import AppLayout from './../../layouts/AppLayout'

// Service
import AuthService from './../../services/AuthService'
import firebase from './../../services/firebase'

// Recaptcha
delete window.recaptchaVerifier;
window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha', {
  size: 'invisible',
});


class Signin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      phone: '+65',
      confirmation: ''
    }
  }

  componentDidMount() {
    let location = queryString.parse(window.location.search);
    if (location.source && location.source !== 'undefined') {
      console.log('SIGNIN SOURCE SET', location.source)
      store.set('source', location.source)
    }
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    recaptchaVerifier
      .verify()
      .then(res=> {
        AuthService()
          .Authenticate(this.state.phone)
          .then(confirmationResult => {
            if (!confirmationResult) {
              Promise.reject()
              this.setState({isError: true})
              return
            }
            setTimeout(()=>{
              console.log('RESPONSE FROM', confirmationResult)
              this.setState({confirmationResult, isError: false})

            }, 500)
          })
      })
  }
  onConfirm = (evt) => {
    evt.preventDefault()
    this.state.confirmationResult
      .confirm(this.state.confirmation)
      .then(res=>{
        this.setState({redirect: true, isError: false})
      })
      .catch(err => this.setState({isError: true}))

  }

  handleChange = (evt) => {
    let obj = {}
    obj[evt.target.name] = evt.target.value
    this.setState(obj)
  }

  render = () => {

    if (this.props.user.displayName || this.state.redirect) return <Redirect to={`/`} />
    if (this.props.user.registrationToken || this.state.redirect) return <Redirect to={`/profile`} />
    return(

      <AppLayout >

      <br></br>
        {/* <div className="row">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><a href="/">Krib.co</a></li>
            <li className="breadcrumb-item active">Sign In</li>
          </ol>
        </div> */}

        <div className="row">
          <div className="col-md-6 col-sm-12 col-12">
            <h4>Sign In to Krib</h4>
            {/* <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni possimus cum dicta uia nesciunt animi fugit eius.</p> */}
            {
              this.state.isError &&
              <div className="alert alert-danger">
                <strong>Error!</strong> <br/>
                Sign In Failed. Please try again.
              </div>
            }

            {
              !this.state.confirmationResult
              ?
                <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                    <label>Simply log in with your phone number. A verification code will be sent to you via SMS.</label>
                    <input type="text" value={this.state.phone} onChange={this.handleChange} name="phone" className="form-control" />
                    <div className="spacer-sm"></div>
                  </div>
                  <div className="form-group text-right">
                    <input type="submit" value="Sign In" className="btn btn-lg btn-primary btn-block" />
                  </div>
                </form>
              :
                <form onSubmit={this.onConfirm}>
                  <div className="form-group">
                    <label>Enter the verification code.</label>
                    <input type="text" value={this.state.confirmation} onChange={this.handleChange} name="confirmation" className="form-control" />
                    <p>Did not receive a verification code? Click <a href="#" onClick={()=>this.setState({confirmationResult: false})}>here</a> to try again.</p>
                  </div>
                  <div className="form-group text-right">
                    <input type="submit" value="Sign In" className="btn btn-lg btn-primary btn-block" />
                  </div>
                </form>
            }
            <div className="text-center">
				<p>By signing in you agree to our Terms of Use.</p>
              <Link to="pages/terms" className="btn btn-link">Terms of Use</Link>
              <span className="text-muted"> | </span>
              <Link to="pages/privacy" className="btn btn-link">Privacy Policy</Link>
            </div>
          </div>
          <div className="col-md-6  hidden-sm-down">
            <img src="/public/interior.jpg" className="btn-block" />
          </div>

        </div>
      </AppLayout>
    )
  }

}

const mapStateToProps = state => ({
  user: state.user
})
export default connect(mapStateToProps)(Signin)
