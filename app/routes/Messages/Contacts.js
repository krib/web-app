import React from 'react'
import {Link} from 'react-router-dom'

export default ({conversations, user, currentConversationId}) => (
    <div className="contacts">
      {
        conversations.map(conversation => (
          <Link
            to={`/messages/${conversation.key}`}
            key={conversation.key}
            className={
              conversation.key === currentConversationId
              ? "contact active"
              : "contact"
            }
            >

            <div
              className="contact-avatar"
              style={{backgroundImage: `url(${conversation.user.photoUrl || '/public/images/avatar_placeholder.png'})`}}></div>

            <div className="hidden-md-down">
               {conversation.user.displayName}
               <br/>
               <small>{conversation.lastMessage}</small>
            </div>
          </Link>
        ))
      }
  </div>
)
