import React from 'react'
import {Link} from 'react-router-dom'
import moment from 'moment'

// Services
import ListingsService from './../../services/ListingsService'
import MessagingService from './../../services/MessagingService'

export default class extends React.Component {
  state = {
    messages: [],
    messageInput: '',
    conversation: { sender: {}},
  }

  componentDidMount() {
    console.log('CONV ID', this.props.conversationId)
    console.log('USER ID', this.props.user.id)
    // GET CONVO
    MessagingService()
      .getConversation(this.props.conversationId, this.props.user.id)
      .then(res => {
        this.setState({conversation: res})
      })
    // Read Convo
    MessagingService()
      .readConversation(this.props.conversationId, this.props.user.id)


    ListingsService()
      .listenMessages(this.props.conversationId, this.onMesssagesUpdate.bind(this))
  }

  onMesssagesUpdate(messages) {
    if (this.state.loadingMore) return
    this.setState({messages})
    MessagingService()
      .readConversation(this.props.conversationId, this.props.user.id)
  }


  componentDidUpdate () {
    setTimeout(()=>{
      if (this.state.loadingMore) return
      let el = document.getElementById('conversation');
      el.scrollTop = el.scrollHeight;
    }, 300)

  }

  componentWillUnmount() {
    ListingsService()
      .unlistenMessages(this.props.conversationId)
    MessagingService()
      .readConversation(this.props.conversationId, this.props.user.id)
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    let data = {
      userId: this.props.user.id,
      message: this.state.messageInput,
      conversationId: this.props.conversationId
    }
    MessagingService()
      .createMessage(data)
      .then(res => this.setState({messageInput: ''}))
  }
  handleChange = (evt) => {
    this.setState({messageInput: evt.target.value})
  }

  loadMore = ()  => {
    this.setState({loadingMore: true})
    ListingsService()
      .getMessages(this.props.conversationId, this.state.messages[0].key, 100)
      .then(res => {
        if (!res.length) this.setState({noResults: true})
        console.log(res)
        let m = this.state.messages
        let msgs = res.concat(m)
        this.setState({messages: msgs})

      })
  }

  render() {
    return (
      <div>
        <div className="row hidden-md-up" style={{padding: '0.5rem'}}>
          <div className="col-7">
            {' '}
          </div>
          <div className="col-5 text-right">
            <a href={`tel:${this.state.conversation.sender.phone}`} className=" btn btn-outline-primary btn-sm"><i className="fa fa-fw fa-phone"></i>Call</a>
          </div>
        </div>

      <div className="conversation" id="conversation" >
        {
          this.state.messages.length > 0 &&
          <div className="text-center">
            {
              !this.state.noResults
              ? <small><button className="btn btn-link btn-sm" onClick={this.loadMore}>Load older messages...</button></small>
              : <small className="text-muted">No more messages.</small>
            }

            <div className="spacer"></div>
          </div>
        }

        {
          this.state.messages.map((message, idx)=> (
            <div key={idx} className={ message.sender.key === this.props.user.id ? 'message-wrapper inverse' : 'message-wrapper' }>
              <div className="contact-avatar hidden-sm-down" style={ message.sender.key === this.props.user.id ? {backgroundImage: `url(${this.props.user.photoUrl})`} : {backgroundImage: `url(${message.sender.photoUrl})`} }></div>
              <div className="message">
                {message.text}
                <br/>
                {
                  message.listingId &&
                  <small>
                    <strong>Note:</strong> Message sent from <Link to={`/listing/${message.listingId}`}>this</Link> listing.<br/>
                  </small>
                }
              <small className="message-time">{moment(message.timestamp).fromNow()}</small>
              </div>
            </div>
          ))
        }
      </div>
      <form onSubmit={this.onSubmit}>
        <textarea className="conversation-input" placeholder="Type your message here..." value={this.state.messageInput} onChange={this.handleChange}></textarea>
        <button type="submit" className="btn btn-primary btn-sm btn-send"><i className="fa fa-fw fa-paper-plane-o"></i></button>
      </form>
    </div>
    )
  }
}
