import React from 'react'
import {Link} from 'react-router-dom'
import moment from 'moment'
// Services

import MessagingService from './../../services/MessagingService'
import ListingsService from './../../services/ListingsService'

// Components
import AppLayout from './../../layouts/AppLayout'

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      messages: [],
      messageInput: ''
    }
  }

  componentDidMount() {
    ListingsService()
      .listenMessages(this.props.match.params.id, this.onMesssagesUpdate.bind(this))
  }
  onMesssagesUpdate(messages) {
    this.setState({messages})
  }
  componentWillUnmount() {
    ListingsService()
      .unlistenMessages(this.props.match.params.id)
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    let data = {
      userId: '4sulPA17lRV5jxp5YnIXue2AHSy1',
      message: this.state.messageInput,
      conversationId: this.props.match.params.id
    }
    MessagingService()
      .createMessage(data)
      .then(res => this.setState({messageInput: ''}))
  }

  handleChange = (evt) => {
    this.setState({messageInput: evt.target.value})
  }

  render = () => (
    <AppLayout>
      <h4>Single Conversation</h4>
      <hr/>
      {
        this.state.messages.map((message, idx)=> (
          <div key={idx}>
            {
              message.listingId &&
              <div className="alert alert-info">
                Message sent from this <Link to={`/listing/${message.listingId}`}>this</Link> listing.
              </div>
            }
            {message.senderId && <div>Sent by: {message.sender.displayName}</div>}
            {message.text} --- {moment(message.timestamp).fromNow()}
            <hr/>
          </div>
        ))
      }
      <hr/>
      {/* New message */}
      <form onSubmit={this.onSubmit}>
        <textarea value={this.state.messageInput} onChange={this.handleChange} className="form-control"></textarea>
        <input type="submit" value="Send" className="btn btn-primary"/>
      </form>

    </AppLayout>
  )
}
