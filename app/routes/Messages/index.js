import React from 'react'
import {Redirect, Link, Route} from 'react-router-dom'
import moment from 'moment'
import {connect} from 'react-redux'

// Services
import ListingsService from './../../services/ListingsService'
import MessagingService from './../../services/MessagingService'


// Components
import AppLayout from './../../layouts/AppLayout'
import PleaseWait from './../../components/PleaseWait'

import Contacts from './Contacts'
import Conversation from './Conversation'

class Messages extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      conversations: [],
      users: [],
      isFetching: true,
      data: {}
    }
  }

  componentDidMount() {
    MessagingService()
      .getConversations(this.props.user.id)
      .then(conversations => {
        console.log(conversations)
        this.setState({conversations, isFetching: false})
      })
  }

  render() {
    if (this.state.isFetching) return <PleaseWait />
    if (!this.state.conversations.length) return (<AppLayout>
        You don't have any messages.
      </AppLayout>)
    return (
      <AppLayout>
        <div className="messages">
          <div className="messages-wrapper">
            <div className="row no-gutters ">
              <div className="col-3">
                <Contacts conversations={this.state.conversations} user={this.props.user} users={this.state.users} data={this.state.data} currentConversationId={this.props.match.params.id}/>
              </div>
              <div className="col-9" >
                {
                  this.props.match.params.id
                    ?
                    <Route
                      path="/messages/:conversationId"
                      component={() => <Conversation
                        user={this.props.user}
                        conversationId={this.props.match.params.id}
                      />}
                    />
                    :
                    <div className="conversation text-center d-flex justify-content-center  align-items-center">
                      <div>
                        <h4>Your Messages</h4>
                        <p className="text-muted">Select a conversation from the menu.</p>
                      </div>
                    </div>
                }

              </div>
            </div>
          </div>
        </div>
      </AppLayout>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps, null, null, { withRef: true })(Messages)
