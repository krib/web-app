import React from 'react'
import {Redirect} from 'react-router-dom'
import store from 'store'
import {connect} from 'react-redux'
import queryString from 'query-string'
import { goToAnchor, configureAnchors } from 'react-scrollable-anchor'


import {showLoginForm} from './../../reducers/loginReducer'

import AppLayout from './../../layouts/AppLayout'
import {housingTypes, leaseTypes, leaseLengths} from './../../services/StaticModelsService'
import ListingForm from './components/ListingForm'
import PhotoManager from './components/PhotoManager'

import ListingsService from './../../services/ListingsService'
import ListingService from './../../services/ListingService'


class ManageListing extends React.Component {
  state = {
    listing: {
      address: {}
    }
  }

  componentDidUpdate() {
    if (!this.state.createdId) return false
    const location = queryString.parse(window.location.search);
    const scrollToPhotos = location.photos
    console.log(scrollToPhotos)
    if (scrollToPhotos) {
      setTimeout(()=>{
        goToAnchor('photos')
      },500)
    }
  }

  componentDidMount() {
    // Only if in edit mode
    if (!this.props.match.params.id) return
    this.fetchListing()
    // Listen Photos
    ListingsService()
      .listenPhotos(this.props.match.params.id, this.fetchListing)
  }
  componentWillUnmount() {
    ListingsService()
      .unlistenPhotos(this.props.match.params.id)
  }

  fetchListing = () => {
    ListingsService()
      .getListing(this.props.match.params.id)
      .then(listing => this.setState({ listing }))
  }

  onSubmit = (data) => {
    // Check if user is logged in
    if (!this.props.user.displayName) {
      // let toStore = Object.assign({}, data)
      // if (!toStore.address) toStore.address = {}
      // toStore.rent = data.rent * 100
      // toStore.leaseType = { key: toStore.leaseType }
      // toStore.housingType = { key: toStore.housingType }
      // toStore.leaseLengthPref = { key: toStore.leaseLengthPref }
      // store.set('tempListing', toStore)
      this.props.showLoginForm()
      //this.setState({redirectToSignin: true})
      return
    }

    // store.remove('tempListing')

    if (this.props.match.params.id) {
      // Update
      data.listingId = this.props.match.params.id
      ListingService()
        .updateListing(data)
        .then(res => this.setState({redirect: true}))
      return
    } else {
      // Create
      data.listedBy = this.props.user.id
      console.log(data)

      ListingService()
        .createListing(data)
        .then(createdId => {
          console.warn("Created ID" + createdId)
          this.setState({createdId})
          this.setState({createdId: null})
          this.fetchListing()
          ListingsService()
            .listenPhotos(this.props.match.params.id, this.fetchListing)
        })
    }
  }

  render() {
    if (this.state.createdId) return <Redirect to={`/edit-listing/${this.state.createdId}?photos=true`} />
    if (this.state.redirect) return <Redirect to="/my-listings" />

    return (
      <AppLayout>
        {this.state.redirectToSignin && <Redirect to={`/signin?source=/new-listing`} />}
        <h2>{this.props.match.params.id ? 'Edit' : 'Create'} Listing</h2>
		<br></br>
          <p>Please complete the following to create your listing.</p>  
        <ListingForm onSubmit={this.onSubmit} listingId={this.props.match.params.id} listing={this.state.listing} fetchListing={this.fetchListing} step={'address'}/>
      </AppLayout>
    )
  }
}
const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch => ({
  showLoginForm: () => dispatch(showLoginForm())
})
export default connect(mapStateToProps, mapDispatchToProps)(ManageListing)
