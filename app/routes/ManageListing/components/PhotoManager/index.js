import React from 'react'
import PropTypes from 'prop-types'
import Dropzone from 'react-dropzone'

import PhotoService from './../../../../services/PhotoService'
import ListingItem from './../../../../components/ListingItem'


export default class PhotoManager extends React.Component {

  static propTypes = {
    listingId: PropTypes.string.isRequired,
    photos: PropTypes.array.isRequired,
    onAdd: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      isUploading: false
    }
  }

  onDrop = (files, rejected) => {
    let minValue = 0
    if (this.props.photos.length) {
      minValue = this.props.photos[this.props.photos.length-1].key.split('photo')[1]
      minValue = parseInt(minValue)
    }
    this.setState({isUploading: true})
    PhotoService()
      .uploadPhotos(this.props.listingId, files, minValue)
      .then(res => {
          this.setState({isUploading: false})
      })
  }

  handleDelete = (key) => (event) => {
    PhotoService()
      .deletePhoto(this.props.listingId, key)
      .then(res => {
        this.props.onDelete()
      })
  }

  render() {
    return (
      <div>
        <h3 ><span className="badge badge-info">4</span> Photos</h3>
        <p>Almost done! Add some photos to your listing.</p>
        {
          this.state.isUploading
          && <div className="alert alert-warning">Uploading photos...</div>
        }

        {
          this.props.photos.map(photo => (
            <div key={photo.key} style={{display: 'inline-block', maxWidth: '200px', marginRight: '10px', verticalAlign: 'top'}}>
              {photo.thumb_url !== true && <img src={photo.thumb_url} style={{width: '100%', marginBottom:'10px'}}/>}
              {photo.thumb_url !== true && this.props.photos.length > 1 && <span className="btn btn-outline-primary btn-sm" style={{cursor: 'pointer'}} onClick={this.handleDelete(photo.key)}>Delete</span>}
            </div>
          ))
        }

        {
          this.props.photos.length < 5 && !this.state.isUploading &&
          <div className="form-group" style={{display: 'inline-block', verticalAlign: 'top'}}>
              <Dropzone onDrop={this.onDrop} accept="image/*" className="dropzone">
                Upload new photo
              </Dropzone>
          </div>
        }

      </div>
    )
  }

}
