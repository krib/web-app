import React from 'react'
import { Link } from 'react-router-dom'
import AppLayout from './../../../../layouts/AppLayout'
import Address from './Address'
import Property from './Property'
import Facilities from './Facilities'

import { facilities } from './../../../../services/StaticModelsService'
import ListingsService from './../../../../services/ListingsService'
import PostalCodeService from './../../../../services/PostalCodeService'
import PhotoManager from '../PhotoManager'


export default class ListingForm extends React.Component {
	constructor(props) {
		super(props)
		this.facilities = []
		this.state = {
			address: {
				buildingName: '',
				postalCode: '',
				streetName: '',
			},
			facilities: facilities,
			bathrooms: 0,
			bedrooms: 0,
			rent: 0,
			leaseType: 0,
			housingType: 0,
			leaseLengthPref: 0,
			isValid: true,
		}
	}
	componentWillMount() {
		this.facilities = []
		for (let key in facilities) {
			this.facilities.push(key)
		}
	}

	componentWillReceiveProps(props) {
		console.log('received props', props)
		if (!props.listing.address.postalCode) return
		// Sanitization
		let newState = {}
		newState = {
			address: {
				buildingName: props.listing.address.buildingName || '',
				postalCode: props.listing.address.postalCode || '',
				streetName: props.listing.address.streetName || '',
			},
			photos: props.listing.photos || {},
			bathrooms: props.listing.bathrooms || 1,
			bedrooms: props.listing.bedrooms || 1,
			rent: parseInt(props.listing.rent) / 100 || 0,
			leaseType: props.listing.leaseType ? props.listing.leaseType.key : 0,
			housingType: props.listing.housingType ? props.listing.housingType.key : 0,
			leaseLengthPref: props.listing.leaseLengthPref ? props.listing.leaseLengthPref.key : 0,
			published: props.listing.published || null,
		}
		if (props.listing.address.lat) newState.address.lat = props.listing.address.lat
		if (props.listing.address.lng) newState.address.lng = props.listing.address.lng
		if (props.listing.facilities) newState.facilities = { ...this.state.facilities, ...props.listing.facilities }
		this.setState(newState)

	}

	onSubmit = (event) => {
		event.preventDefault()
		let form = Object.assign({}, this.state)
		delete form.isFetching
		delete form.isValid
		delete form.published
		this.props.onSubmit(form)
	}

	updateState = (state) => {
		this.setState(state)
	}

	fetchAddress(field) {
		console.warn('Should Fetch Address', field)
		this.setState({ isFetching: true, isValid: true })
		PostalCodeService()
			.getPostalCodeData(field)
			.then(res => {

				let address = this.state.address

				if (res.buildingName) address.buildingName = res.buildingName
				else address.buildingName = ''

				if (res.streetName) address.streetName = res.streetName
				else address.streetName = ''

				if (res.lat) address.lat = res.lat
				if (res.lon) address.lng = res.lon

				this.setState(address)
				this.setState({ isFetching: false, isValid: true })

			})
			.catch(err => {
				let address = this.state.address
				address.buildingName = ''
				address.streetName = ''
				address.postalCode = ''
				this.setState(address)
				this.setState({ isFetching: false, isValid: false })
			})
	}

	handleChange = (key, field) => (event) => {
		if (key === 'address' && field === 'postalCode' && event.target.value.length === 6) {
			this.fetchAddress(event.target.value)
		}
		if (key === 'address' && field === 'postalCode' && event.target.value.length > 6) return
		let newState = this.state
		if (field) {
			newState[key][field] = event.target.value;
			this.setState({ ...newState })
			return
		}
		newState[key] = event.target.value;
		this.setState({ ...newState })
	}

	toggleCheckbox = (key) => (evt) => {
		console.log(key)
		let obj = {}
		obj[key] = !this.state.facilities[key]
		this.setState({ facilities: { ...this.state.facilities, ...obj } })
	}

	render = () => (
		<form onSubmit={this.onSubmit}>
			<hr className="hr-lg" />
			<Address address={this.state.address} handleChange={this.handleChange} isValid={this.state.isValid} isFetching={this.state.isFetching} />
			<hr className="hr-lg" />
			<Property
				rent={this.state.rent}
				leaseType={this.state.leaseType}
				housingType={this.state.housingType}
				leaseLengthPref={this.state.leaseLengthPref}
				bedrooms={this.state.bedrooms}
				bathrooms={this.state.bathrooms}
				handleChange={this.handleChange}
				updateState={this.updateState}
			/>
			<hr className="hr-lg" />
			<Facilities
				facilityList={this.facilities}
				facilities={this.state.facilities}
				toggleCheckbox={this.toggleCheckbox}
			/>
			<hr className="hr-lg" />
			{this.props.listing.photos &&
				<div>
					<PhotoManager photos={this.props.listing.photos} listingId={this.props.listing.key} onAdd={this.props.fetchListing} onDelete={this.props.fetchListing} />
					<hr className="hr-lg" />
				</div>
			}
			<div className="form-group text-right">


				{
					!this.state.isFetching && this.state.isValid && this.state.address.postalCode.toString().length > 5
						? <input type="submit" value='Save' className="btn btn-primary btn-lg" />
						: <div className="btn btn-primary btn-lg disabled" disabled>Save</div>
				}


			</div>
		</form>
	)
}
