import React from 'react'

export default  ({address, handleChange, isValid, isFetching})  => (
  <div>
    <h4><span className="badge badge-info"><div style={{transform: 'translateX(2px)'}}>1&nbsp;</div></span> Address</h4>
    <p>Enter your postal code.</p>
    <div className="row">
      <div className="form-group col-sm-2">
        <label>Postal Code</label>
        <input type="number" className="form-control" value={address.postalCode} min="100000" maxLength='6' onChange={handleChange('address', 'postalCode')} required/>
      </div>
      <div className="form-group col-sm-4">
        <label>Building Name</label>
        <input type="text" className="form-control" value={address.buildingName} onChange={handleChange('address', 'buildingName')} disabled required/>
      </div>
      <div className="form-group col-sm-6">
        <label>Street Name</label>
        <input type="text" className="form-control" value={address.streetName} onChange={handleChange('address', 'streetName')} disabled required/>
      </div>
    </div>
    {
      !isValid &&
      <div className="alert alert-danger">
        Postal Code is Not Valid, please try again.
      </div>
    }
    {
      isFetching &&
      <div className="alert alert-info">
        <i className="fa fa-fw fa-spinner fa-spin"></i> Fetching Postal Code
      </div>
    }

  </div>

)
