import React from 'react'
import {housingTypes, leaseTypes, leaseLengths} from './../../../../services/StaticModelsService'


export default ({rent, handleChange, updateState, leaseType, housingType, leaseLengthPref, bedrooms, bathrooms }) => (
  <div>
    <h3><span className="badge badge-info">2</span> Property Info & Rent</h3>
    <p>Select the correct type of housing and preferred lease period. If you are renting out an entire unit, please also select the number of bedrooms and bathrooms.</p>
    <div className="row">
      <div className="col-sm-3">
        <label>Rent <span className="text-muted">(in S$)</span> <span className="text-danger">*</span></label>
        <input type="number"  min="1" className="form-control" value={rent} onChange={handleChange('rent')} required/>
      </div>
      <div className="col-sm-3">
        <label>Lease Type <span className="text-danger">*</span></label>
        <ul className="list-group">
          {leaseTypes.map((item, idx) =>
            <li
              key={item.key}
              className={leaseType === item.key ? "list-group-item active" : "list-group-item"}
              onClick={() => updateState({leaseType: item.key}) }>{item.name}</li>
            )}
        </ul>
      </div>
      <div className="col-sm-3">
        <label>Housing Type <span className="text-danger">*</span></label>
        <ul className="list-group">
          {housingTypes.map((item, idx) =>
            <li
              key={item.key}
              className={housingType === item.key ? "list-group-item active" : "list-group-item"}
              onClick={() => updateState({housingType: item.key}) }>{item.name}</li>
            )}
        </ul>
      </div>
      <div className="col-sm-3">
        <label>Lease Length <span className="text-danger">*</span></label>
        <ul className="list-group">
          {leaseLengths.map((item, idx) =>
            <li
              key={item.key}
              className={leaseLengthPref === item.key ? "list-group-item active" : "list-group-item"}
              onClick={() => updateState({leaseLengthPref: item.key}) }>{item.name}</li>
            )}
        </ul>
      </div>
    </div>
    <div className="spacer-sm"></div>
    {
      leaseType === 2 &&
      <div className="row">
        <div className="col-3"></div>
        <div className="col-3">
          <div className="row">
            <div className="form-group col-sm-6">
              <label>Bedrooms <span className="text-danger">*</span></label>
              <input type="number" className="form-control" min="1" value={bedrooms} onChange={handleChange('bedrooms')} required/>
            </div>
            <div className="form-group col-sm-6">
              <label>Bathrooms <span className="text-danger">*</span></label>
              <input type="number" className="form-control" min="1" value={bathrooms} onChange={handleChange('bathrooms')} required/>
            </div>
          </div>
        </div>
      </div>
    }
  </div>
)
