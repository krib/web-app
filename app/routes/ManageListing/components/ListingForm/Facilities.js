import React from 'react'

export default ({facilityList, toggleCheckbox, facilities }) => (
  <div>
    <h3><span className="badge badge-info">3</span> Facilities</h3>
    <p>Select the facilities that are available to your tenant.</p>
    {
      facilityList.map((key, idx) => (
        <span
          className={ facilities[key] ? 'btn btn-sm btn-primary btn-toggle' : 'btn btn-sm btn-outline-primary btn-toggle'}
          key={idx}
          onClick={toggleCheckbox(key)}
          >{key}</span>
      ))
    }
    <span id={'photos'}></span>
  </div>

)
