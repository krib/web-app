import React from 'react'
import AppLayout from './../../layouts/AppLayout'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

import ListingsService from './../../services/ListingsService'
import CurateService from './../../services/CurateService'

import {CURATED} from './../../constants/listingConstants'
import CuratedListingsComponent from './components/CuratedListings'

class CuratedListings extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      curatedListings: []
    }
  }

  componentDidMount() {
    ListingsService()
      .getListings(CURATED, this.props.user.id)
      .then(curatedListings => this.setState({ curatedListings }))
    CurateService()
      .getCurationRequest(this.props.user.id)
      .then(res => {
        if (res) this.setState({hasRequest: true})
        console.log(this.state)
      })
  }

  render = () => {
    return (
      <AppLayout>
        <CuratedListingsComponent listings={this.state.curatedListings} hasRequest={this.state.hasRequest} />
      </AppLayout>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(CuratedListings)
