import React from 'react'
import ListingItem from './../../../components/ListingItem'
import { Link } from 'react-router-dom'

const Listings = ({ listings, hasRequest }) => (
	<div>
		<h2>PropertyMatch</h2>
		<br></br>
		{
			!hasRequest &&
			<p>Tell us about your ideal home to start receiving a daily list of property suggestions.</p>
		}
		{
			!listings.length && hasRequest &&
			<div>
				<p>We've started the search! This page will be updated daily with the best property matches for you.</p>
				<p>Keep your email address up to date on your <a href="/profile">Profile</a>. You will receive a notification when we update this page.</p>
			</div>
		}
		{
			listings.length > 0 &&
			<p>Your shortlist of properties.</p>
		}
		<div className="row">
			{
				listings.map((listing, idx) =>
					listing.photos.length
						? <ListingItem key={listing.key} listing={listing} />
						: null
				)
			}
		</div>
		<div className="text-right">
			{
				hasRequest
					? <Link to="/settings" className="btn btn-outline-primary">Update Preferences</Link>
					: <Link to="/settings" className="btn btn-primary">Get Started</Link>
			}
		</div>

	</div>
)

export default Listings
