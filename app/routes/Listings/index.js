import React from 'react'

// Services
import ListingsService from './../../services/ListingsService'
import { areas as AVAILABLE_AREAS } from './../../services/AreasService'

import * as C from './../../constants/listingConstants'
import { LISTING_CONFIG } from './../../app.config.js'

// Components
import AppLayout from './../../layouts/AppLayout'
import Listings from './../../components/Listings'
import PleaseWait from './../../components/PleaseWait'
import RenderHtml from './../../components/RenderHtml'
import FooterBanner from './components/FooterBanner'


export default class Home extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			listings: [],
			isFetching: true,
		}
	}

	componentDidMount() {
		ListingsService()
			.getListings(this.props.match.params.filter, this.props.match.params.id)
			.then(listings => this.setState({ listings, isFetching: false }))
			.then(res => console.log(this.state))
	}

	loadMore = () => {
		let lastKey = (this.state.listings[this.state.listings.length - 1].publishedDate * -1) + 1
		ListingsService()
			.getListings(this.props.match.params.filter, this.props.match.params.id, lastKey, LISTING_CONFIG.perPage)
			.then(listings => {
				if (listings.length < LISTING_CONFIG.perPage) {
					this.setState({ noResults: true })
				}
				let ctx = this.state.listings
				listings.map(item => ctx.push(item))
				this.setState({ listings: ctx, isFetching: false })
			})
	}

	render() {
		if (this.props.match.params.filter === 'area' && AVAILABLE_AREAS.indexOf(this.props.match.params.id) === -1) return <AppLayout>Not Found</AppLayout>
		let breadcrumbs = []
		// Params
		let filter = this.props.match.params.filter
		let type = this.props.match.params.id
		let title = 'Listings'
		switch (filter) {
			case C.LANDLORD:
				title = 'Listings by Landlord'
				breadcrumbs.push({ name: title })
				break;
			case C.AREAS:
				title = `${type}`
				breadcrumbs.push({ name: title })
				break;
			case C.HOUSING:
				if (type === C.LANDED_HOUSE) title = 'Landed Houses'
				if (type === C.CONDOMINIUM) title = 'Condominiums'
				if (type === C.HDB_APARTMENT) title = "HDB Apartments"
				breadcrumbs.push({ name: title })
				break;
			case C.LEASE:
				if (type === C.COMMON_ROOM) title = 'Common Rooms'
				if (type === C.MASTER_ROOM) title = 'Master Rooms'
				if (type === C.ENTIRE_PLACE) title = 'Entire Places'
				breadcrumbs.push({ name: title })
				break;
			case C.CURATED:
				breadcrumbs.push({ name: `Property Match` })
				break;
			default:
				breadcrumbs.push({ name: `Listings` })
				break;
		}

		// if (this.state.isFetching) return  <PleaseWait />
		return (
				<AppLayout>
					<h2>{title}</h2>
					<br></br>
					{/* Render HTML */}
					{/* <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius id accusantium ratione, reiciendis suscipit eum nemo voluptatum tempora eaque quod rerum officiis veniam saepe quis quos natus asperiores sint mollitia.</p> */}

					<div className="spacer"></div>

					<Listings listings={this.state.listings} loadMore={this.loadMore} noResults={this.state.noResults} isFetching={this.state.isFetching} />
					<div className="spacer"></div>
					<div className="spacer"></div>
					<div className="spacer"></div>
					<hr className="hr-lg"/>
					<FooterBanner />
					{
						filter === C.AREAS &&
						<RenderHtml url={`/areas/${type}`} />
					}
				</AppLayout>
		)
	}
}
