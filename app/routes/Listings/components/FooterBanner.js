import React from 'react'
import {Link} from 'react-router-dom'
export default () => (
  <div>
    {/* <div className="spacer-lg"></div> */}
    <div className="section" style={{backgroundImage: `url(/public/images/white.jpeg)`}}>
      {/* <div className="section-overlay"></div> */}
      <div className="container section-container">
        <h2 style={{color: `#003660`}}>Couldn't find what you are looking for?</h2>
        <p style={{color: `#00579A`}}>There is a better way to discover property. Let us find the perfect home for you.</p>
        {/* <Link to="/settings" className="btn btn-outline-secondary btn-lg">Try It Now</Link> */}
		<Link to="/settings" className="btn btn-primary btn-lg">Try PropertyMatch</Link>
      </div>
    </div>
  </div>
)
