import React from 'react'
import {Link, Redirect} from 'react-router-dom'
import moment from 'moment'
import {connect} from 'react-redux'
import store from 'store'
import {fetchUser, setFirebaseUser, fetchUserPhoto} from './../../actions/userActions'

// Services
import UsersService from './../../services/UsersService'
import CurateService from './../../services/CurateService'
import AppLayout from './../../layouts/AppLayout'
import Dropzone from 'react-dropzone'

class Profile extends React.Component {
  state = {
    isFetching: false,
    displayName: '',
    email: '',
    hasPhoto: true
  }

  componentDidMount() {
    this.source = store.get('source')
    this.props.fetchUser(this.props.user.id)
    this.props.fetchUserPhoto(this.props.user.id)
    console.warn('PROFILE RECEIVED SOURCE', this.source)
  }

  componentWillReceiveProps(props) {
    if (props.user.displayName) {
      this.setState({
        displayName: props.user.displayName,
        email: props.user.email
      })
    }
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    // Check if there's photo
    this.setState({hasPhoto: true})
    if (!this.props.user.photoUrl) {
      this.setState({hasPhoto: false})
      return
    }
    let data = {
      displayName: this.state.displayName,
      email: this.state.email,
      phoneNumber: this.props.user.auth.phoneNumber,
      id: this.props.user.id
    }
    // Create User
    const updateObj = {}
    UsersService()
      .updateUser(data)
      .then(res => {
        this.props.fetchUser(this.props.user.id)
        this.setState({isSaved: true})
      })
  }


  handleChange = (evt) => {
    let upd = {}
    upd[evt.target.name] = evt.target.value
    this.setState(upd)
  }

  onDrop = (files) => {
    if (!files) return
    UsersService()
      .uploadPhoto(this.props.user.id, files[0])
      .then(res => this.props.fetchUserPhoto(this.props.user.id))
  }

  render = () => {
    if (!this.props.user.auth) return <AppLayout>Please wait</AppLayout>
    if (this.props.user.id && this.source) {
      store.remove('source')
      return <Redirect to={this.source} />
    }
    return(
      <AppLayout>
        <h2>My Profile</h2>
        <p>Do make sure your profile is up to date. If you wish to change your phone number, please email us at <a href="mailto:hello@krib.co?Subject=Request%20Number%20Change" target="_top">hello@krib.co</a>.</p>
        <div className="spacer"></div>

        {!this.props.user.displayName &&
          <div className="alert alert-warning">In order to create listings and contact landlords please update your profile first (full name, photo and email is required).</div>
        }

        {
          !this.state.hasPhoto && <div className="alert alert-danger">
          <strong>Error!</strong> Please upload profile image before saving.
        </div>
        }

        <div className="row">
          <div className="col-md-2 ">
            <h5>Photo</h5>
            {
              this.props.user.photoUrl
              ?
                <div className="text-center">
                  <div className="profile-photo" style={{backgroundImage: `url(${this.props.user.photoUrl})`}}></div>
                  <div className="spacer-sm"></div>
                  <Dropzone onDrop={this.onDrop} accept="image/jpeg" className="btn btn-outline-primary btn-sm">
                    Change Photo
                  </Dropzone>
                </div>

              :
                <Dropzone onDrop={this.onDrop} accept="image/*" className="dropzone">
                  <div>
                    <h6>Upload</h6>
                    Tap or drop photo here to upload
                  </div>
                </Dropzone>
            }
            <div className="spacer-sm"></div>
            <small className="text-muted">Please upload square-size photo. Optimal resolution <code>640x640px</code>. Only <code>jpg</code> photos are allowed.</small>


          </div>
          <div className="col-md-10 ">
            <h5>Basic Info</h5>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label>Full Name <span className="text-danger">*</span></label>
                <input type="text" className="form-control" name="displayName" value={this.state.displayName} onChange={this.handleChange} required/>
              </div>
              <div className="form-group">
                <label>Email <span className="text-danger">*</span></label>
                <input type="email" className="form-control" name="email" value={this.state.email} onChange={this.handleChange} required/>
              </div>
              <div className="form-group">
                <label>Phone Number</label>
                <input type="text" className="form-control" name="phoneNumber" value={this.props.user.auth.phoneNumber} disabled />
              </div>
              <div className="form-group text-right">
                {
                  this.state.isSaved
                  ? <input type="submit" value="Saved" className="btn btn-lg btn-primary btn-block"/>
                  : <input type="submit" value="Save" className="btn btn-lg btn-primary btn-block"/>
                }

              </div>

            </form>
          </div>

        </div>
      </AppLayout>
    )
  }

}

const mapStateToProps = state => ({
  user: state.user
})


const mapDispatchToProps = (dispatch) => ({
  fetchUser: (userId) => dispatch(fetchUser(userId)),
  fetchUserPhoto: userId =>dispatch(fetchUserPhoto(userId))
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
