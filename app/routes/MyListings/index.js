import React from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'

// Services
import ListingsService from './../../services/ListingsService'
import ListingService from './../../services/ListingService'


// Components
import AppLayout from './../../layouts/AppLayout'
import PleaseWait from './../../components/PleaseWait'


class MyListings extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      listings: [],
      isFetching: true,
    }
  }

  componentDidMount() {
    console.log(this.props.match.filter)
    ListingsService()
      .getListingsDetailed('landlord', this.props.user.id)
      .then(listings => this.setState({ listings, isFetching: false }))
  }

  togglePublish = (key) => evt => {
    let upd = this.state.listings
    let state = true
    upd = upd.map(listing => {
      if (listing.key === key) {
        listing.published = !listing.published
        state = listing.published;
      }
      return listing
    })
    this.setState({listings: upd})
    ListingService()
      .publishListing(key, state)
  }

  render() {
    if (this.state.isFetching) return  <PleaseWait />
    return (
      <AppLayout>
        <div className="row d-flex justify-content-around align-items-center">
          <div className="col-sm-6"><h2>My Properties</h2></div>
          <div className="col-sm-6 text-right"><Link to="/new-listing" className="btn btn-primary btn-lg">New Listing</Link></div>
        </div>
        <div className="spacer"></div>
        <p>Manage your property listings.</p>
        {
          this.state.listings.length === 0 && !this.state.isFetching &&
          <p className="text-center">
            <div className="spacer"></div>
            {/* <h5>Create your first listing</h5> */}
            <p>Create your first listing! Click <code>New Listing</code> to get started.</p>
          </p>
        }
        {
          this.state.listings.length > 0 &&
          <table className="table table-striped">
            <thead>
              <tr>
                <th style={{width: '10px'}}>#</th>
                <th>Address</th>
                <th>Info</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.listings.map((listing,idx) =>(
                  <tr key={idx}>
                    <td>{idx+1}</td>
                    <td>
                      <strong className="text-info">{listing.address.streetName}</strong>, <br/>
                      {listing.address.buildingName}, {listing.address.postalCode} <br/>
                      {listing.area}
                    </td>
                    <td>
                      {listing.leaseType.name}
                      <span className="text-muted">/</span>
                      {listing.housingType.name} <br/>
                      {listing.leaseLengthPref.name} <br/>
                      <strong className="text-info">${(Math.round(listing.rent / 100)).toLocaleString()} {' '}</strong>
                    </td>
                    <td style={{verticalAlign: 'middle'}}>
                      {
                        listing.published
                        ? <strong className="text-success">Published</strong>
                        : <strong className="text-danger">Unpublished</strong>
                      }
                      {
                        listing.photos.length === 0 &&
                        <div>
                          <small className="text-muted">Cannot Publish, add photos first</small>
                        </div>
                      }
                      {
                        listing.photos.length !== 0 && listing.published === undefined &&
                        <div>
                          <small className="text-muted">Under Review</small>
                        </div>
                      }

                    </td>
                    <td style={{width: '305px', verticalAlign: 'middle'}}>
                      <Link to={`/listing/${listing.key}`} className="btn btn-info btn-sm btn-toggle">Preview</Link>
                      <Link to={`/edit-listing/${listing.key}`} className="btn btn-info btn-sm btn-toggle">Edit</Link>
                      {
                        listing.published &&
                        <button className="btn btn-danger btn-sm btn-toggle" onClick={this.togglePublish(listing.key)}>Unpublish</button>
                      }
                      {
                        listing.published === false &&
                        <button className="btn btn-primary btn-sm btn-toggle" onClick={this.togglePublish(listing.key)}>Publish</button>
                      }
                      {
                        listing.published === undefined &&
                        <button className="btn btn-primary btn-sm btn-toggle" disabled>Publish</button>
                      }


                      <br/>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        }

      </AppLayout>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(MyListings)
