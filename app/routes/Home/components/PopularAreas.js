import React from 'react'
import {Link} from 'react-router-dom'
import AreasService from './../../../services/AreasService'
import {POPULAR_AREAS_CONFIG} from './../../../app.config.js'


const links = []
let i;

const housingTypes = [
  {name: 'HDB Apartments', url: 'listings/housing/hdbApartment', description: 'State-built apartments that make up the majority of Singapore\'s housing market.', alias: 'hdbApartment', backgroundUrl: '/public/images/hdbApartment.jpg'},
  {name: 'Condominiums', url: 'listings/housing/condominium', description: 'Private developments that usually come with facilities like swimming pools and gyms.', alias: 'condominium', backgroundUrl: '/public/images/condominium.jpg'},
  {name: 'Landed Houses', url: 'listings/housing/landedHouse', description: 'Private housing that occupies its own plot of land, and may come with a garden.', alias: 'landedHouse', backgroundUrl: '/public/images/landedHouse.jpg'},
]

const leaseTypes = [
  {name: 'Common Rooms', url: '/listings/lease/commonRoom', description: 'A private bedroom in a shared house, usually with a single sized bed and a shared bathroom.', alias: 'commonRoom', backgroundUrl: '/public/images/commonRoom.jpg'},
  {name: 'Master Rooms', url: '/listings/lease/masterRoom', description: 'A private bedroom in a shared house, usually with a queen sized bed and an attached bathroom.', alias: 'masterRoom', backgroundUrl: '/public/images/masterRoom.jpg'},
  {name: 'Entire Places', url: '/listings/lease/entirePlace', description: 'A full apartment or landed house that may be furnished or unfurnished.', alias: 'entirePlace', backgroundUrl: '/public/images/entirePlace.jpg'},

]

export default class extends React.Component {
  state = {
    byArea: [],
    byHousingType: [],
    byLeaseType: [],
    canLoadMore: true,
  }

  componentDidMount() {
    AreasService()
      .getCounts()
      .then(res => {

        for (i = 0; i < 7; i++) {
          res.byArea[i].visible=true
          if (res.byArea[i]) {
            res.byArea[i].visible=true
          } else {
            this.setState({canLoadMore: false})
          }
        }
        this.setState(res)
        console.log(res)
      })
  }

  loadMore = () => {
    let arr = this.state.byArea
    let close = i + 7;
    for (i; i < close; i++) {
      if (arr[i]) {
        arr[i].visible=true
      } else {
        this.setState({canLoadMore: false})
      }
    }
    this.setState({byArea: arr})
  }
  render() {
    return (
      <div>
        <hr className="hr-lg"/>
		<br/>
        <h2>Popular Locations</h2>
          <br/>
        <div className="row">
          {
            this.state.byArea.map((link,idx) => {
              if (!link.visible) return null
            return (
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12" key={idx}>
                {
                  link.visible &&
                    <Link to={`/listings/area/${link.name}`}>
                      <div className="listing-area" style={{backgroundImage: `url(/public/images/areas/${link.name.replace(/\s/g, '')}.jpg)`}}>
                        <div className="listing-area-overlay" style={{opacity: POPULAR_AREAS_CONFIG.overlayOpacity}}></div>
                        <div className="listing-area-caption" style={{marginTop: '0rem', height:'100%'}}>
                          <div>
                            {link.name} <br/>
                            <small>{link.description}</small>
                             <small>{link.count} {link.count === 1 ? 'listing' : 'listings'}</small>
                          </div>
                        </div>
                      </div>
                    </Link>
                }
              </div>)
            })
          }
          {/* Load More */}
          {
            this.state.canLoadMore &&
            <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12" onClick={this.loadMore}>
              <div className="listing-area" style={{background: '#00A699'}}>
                <div className="listing-area-overlay" style={{opacity: POPULAR_AREAS_CONFIG.overlayOpacity}}></div>
                <div className="listing-area-caption" style={{marginTop: '0rem', height:'100%'}}> LOAD MORE </div>
              </div>
            </div>
          }
        </div>
		<br/>
        <hr className="hr-lg"/>
        <h2>Categories</h2>
		<br/>
        <div className="spacer"></div>
        <div className="row">
          {
            housingTypes.map((link,idx) => (
              <div key={idx} className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <Link to={link.url}>
                  <div className="listing-area" style={{backgroundImage: `url(${link.backgroundUrl})`}}>
                    <div className="listing-area-overlay" style={{opacity: POPULAR_AREAS_CONFIG.overlayOpacity}}></div>
                    <div className="listing-area-caption">
                      <div>
                        {link.name} <br/>
                        <small>{link.description}</small>
                         {/* <br/> <small>{this.state.byHousingType[link.alias] || '0'} {this.state.byHousingType[link.alias] === 1 ? 'listing' : 'listings'}</small> */}
                      </div>
                    </div>
                  </div>
                </Link>

              </div>
            ))
          }
        </div>

        <div className="row">
          {
            leaseTypes.map((link,idx) => (
              <div key={idx} className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <Link to={link.url}>
                  <div className="listing-area" style={{backgroundImage: `url(${link.backgroundUrl})`}}>
                    <div className="listing-area-overlay" style={{opacity: POPULAR_AREAS_CONFIG.overlayOpacity}}></div>
                    <div className="listing-area-caption">
                      <div>
                        {link.name} <br/>
                        <small>{link.description}</small>
                        {/* {link.name} <br/> <small>{this.state.byLeaseType[link.alias] || '0'} {this.state.byLeaseType[link.alias] === 1 ? 'listing' : 'listings'}</small> */}
                      </div>
                    </div>
                  </div>
                </Link>

              </div>
            ))
          }

        </div>
      </div>
    )
  }
}
