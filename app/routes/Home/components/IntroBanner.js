import React from 'react'
import { Link } from 'react-router-dom'
import queryString from 'query-string'

import { HOME_BANNER } from './../../../app.config.js'

export default class extends React.Component {

  state = {
    source: 'default'
  }
  componentDidMount() {
    let location = queryString.parse(window.location.search);
    let source = (location.source) ? location.source : 'default'
    this.setState({source: source})
  }

  render() {
    return (
      <div className="section" style={{backgroundImage: `url(${HOME_BANNER[this.state.source].backgroundUrl})`}}>
        <div className="section-overlay"></div>
        <div className="container section-container" style={{paddingTop: '14rem', paddingBottom: '14rem'}}>
          <h2>{HOME_BANNER[this.state.source].title}</h2>
          <p>{HOME_BANNER[this.state.source].description}</p>
          <Link to={HOME_BANNER[this.state.source].ctaUrl} className="btn btn-outline-secondary btn-lg">{HOME_BANNER[this.state.source].ctaCaption}</Link>
        </div>
      </div>
    )
  }
}
