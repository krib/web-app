import React from 'react'
import {Link} from 'react-router-dom'
export default () => (
  <div>
    <div className="spacer-lg"></div>
    <div className="section" style={{backgroundImage: `url(/public/images/agentBanner.jpeg)`}}>
      <div className="section-overlay"></div>
      <div className="container section-container">
        <h2>Haven't Found the Perfect Place Yet?</h2>
        <p>Try PropertyMatch. We search through thousands of listings daily to find the perfect match for you.</p>
        <Link to="/settings" className="btn btn-outline-secondary btn-lg">Try It Now</Link>
      </div>
    </div>
  </div>
)
