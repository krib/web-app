import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
// Services
import ListingsService from './../../services/ListingsService'

import { CURATED } from './../../constants/listingConstants'

// Components
import HomeLayout from './../../layouts/HomeLayout'
import Listings from './../../components/Listings'
import PopularAreas from './components/PopularAreas'
import CuratedListings from './../CuratedListings/components/CuratedListings'
import PleaseWait from './../../components/PleaseWait'
import IntroBanner from './components/IntroBanner'
import FooterBanner from './components/FooterBanner'
import OneTwoThree from './../../components/OneTwoThree'


class Home extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			listings: [],
			curatedListings: [],
			isFetching: true,
			oneTwoThreeItems: [
				{
					title: 'Make A Curated Listing Request',
					description: 'Tell us what you\'re looking for in your house hunt, and we\'ll search for suitable listings across Singapore.',
					image: '/public/icon1.svg'
				},
				{
					title: 'Get A Daily Report Of Recommended Properties',
					description: 'Receive 5 suggested listings matching your requirements daily, and use Krib to contact the agent or owner.',
					image: '/public/icon2.svg'
				},
				{
					title: 'Find Your Dream Home Effortlessly',
					description: 'Instead of sorting through thousands of listings, Krib saves you time and effort by doing all the hard work. You just need to view the property and decide!',
					image: '/public/icon3.svg'
				}				
			]
		}
	}

	componentDidMount() {
		ListingsService()
			.getListings()
			.then(listings => this.setState({ listings, isFetching: false }))
		// Curated Listings
		// ListingsService()
		// 	.getListings(CURATED, this.props.user.id)
		// 	.then(curatedListings => this.setState({ curatedListings }))
	}

	loadMore = () => {
		let lastKey = (this.state.listings[this.state.listings.length - 1].publishedDate * -1) + 1
		ListingsService()
			.getListings(null, null, lastKey, 4)
			.then(listings => {
				if (listings.length < 4) {
					this.setState({ noResults: true })
				}
				let ctx = this.state.listings
				listings.map(item => ctx.push(item))
				this.setState({ listings: ctx, isFetching: false })
			})
	}


	render() {
		// if (this.state.isFetching) return <PleaseWait />
		return (
			<HomeLayout>
				<IntroBanner />
				<div className="spacer-lg"></div>
				<div className="container">
					{/* {this.props.user.id &&
            <div>
              <CuratedListings listings={this.state.curatedListings} />
              <hr className="hr-lg"/>
          </div>} */}
		  			<br/>
		 		 	<OneTwoThree items={this.state.oneTwoThreeItems}/>
					<br></br>
					<hr className="hr-lg"/>
					<br/>
					{!this.state.isFetching && <h2>Latest Listings</h2>}
					<br></br>
					<Listings listings={this.state.listings} loadMore={this.loadMore} noResults={this.state.noResults} isFetching={this.state.isFetching} />
					<br></br>
					<PopularAreas areas={this.state.areas} />
					<br></br>
				</div>
				<FooterBanner />
			</HomeLayout>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user
})

export default connect(mapStateToProps)(Home)
