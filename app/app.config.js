/**
 * CLOUD_FUNCTIONS_HOSTNAME -- PRODUCTION and STAGING
 */

export const CLOUD_FUNCTIONS_HOSTNAME = (process.env.MODE === 'production')
  ? 'https://us-central1-krib-production.cloudfunctions.net'
  : 'https://us-central1-krib-staging.cloudfunctions.net'

/*
* LISTING CONFIGURATION
*/
export const LISTING_CONFIG = {
  perPage: 8, // how many listings to display on initial load (same amout is added on "load more click")
}
/*
* MESSAGING CONFIGURATION
*/
export const MESSAGING_CONFIG = {
  perPage: 50, // how many messages in conversation to display on initial load (same amout is added on "load more click")
}

/*
* HOMEPAGE BANNER CONFIGURATION
* Add "source" query string in header to see variations, eg:
* krib.co/?source=landlord
*/
export const HOME_BANNER = {
  // If there is no `source` query string it will fallback to default one:
  default: {
    title: `Let Us Help You In Your House Hunt!`,
	description: `Overwhelmed by thousands of irrelevant property search results? Krib helps tenants discover only the best and most suitable places to rent in Singapore. Make a PropertyMatch request today and you'll receive a personalised curated shortlist of properties, sent directly to your inbox within 24 hours!`,
    backgroundUrl: `/public/images/defaultBanner.jpeg`,
    ctaCaption: 'Make A PropertyMatch Request',
    ctaUrl: `/settings`
  },
  landlords: {
    title: `Find the Right Tenant`,
    description: `Connect with tenants directly through Krib. Listing your property on Krib is easy and free of charge.`,
    backgroundUrl: `/public/images/landlordBanner.jpeg`,
    ctaCaption: 'List Your Property',
    ctaUrl: `/new-listing`
  },
  tenants: {
    title: `Let Us Help You In Your House Hunt!`,
	description: `Overwhelmed by thousands of irrelevant property search results? Krib helps tenants discover only the best and most suitable places to rent in Singapore. Make a PropertyMatch request today and you'll receive a personalised curated shortlist of properties, sent directly to your inbox within 24 hours!`,
    backgroundUrl: `/public/images/defaultBanner.jpeg`,
    ctaCaption: 'Make A PropertyMatch Request',
    ctaUrl: `/settings`
  },
  agents: {
    title: `No Subscription Fees, No Hidden Charges`,
    description: `Whether you're an agent or a landlord, listing properties on Krib will always be free.`,
    backgroundUrl: `/public/images/agentBanner.jpeg`,
    ctaCaption: 'List Now',
    ctaUrl: `/new-listing`
  }
  // You can keep adding more
  // customers: { ... }
}

export const POPULAR_AREAS_CONFIG = {
  overlayOpacity: '0.35',

}
