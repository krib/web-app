import axios from 'axios'
import { CLOUD_FUNCTIONS_HOSTNAME } from '../app.config'

export default (userId, token) => {

	const url = `${CLOUD_FUNCTIONS_HOSTNAME}/user_api/conversations?with=${userId}&create=true`

	return axios({
		url: url,
		method: 'get',
		headers: { 'Authorization': `Bearer ${token}` }
	})
		.then(res => res)
}
