import firebase from './firebase'
import moment from 'moment'
import GeoFire from 'geofire'

const db = firebase.database()

export default () => {
  const listing = {
    updateListing,
    createListing,
    publishListing
  }

  function updateListing(data) {
    if (!data.listingId) return
    const updateObj = {}
    const path = `listings/${data.listingId}`
    // Address
    updateObj[`${path}/address/buildingName`] = data.address.buildingName
    updateObj[`${path}/address/postalCode`] = parseInt(data.address.postalCode)
    updateObj[`${path}/address/streetName`] = data.address.streetName

    // Basic
    updateObj[`${path}/rent`] = parseInt(data.rent) * 100
    updateObj[`${path}/bedrooms`] = parseInt(data.bedrooms)
    updateObj[`${path}/bathrooms`] = parseInt(data.bathrooms)
    // Selects
    updateObj[`${path}/leaseType`] = parseInt(data.leaseType)
    updateObj[`${path}/leaseLengthPref`] = parseInt(data.leaseLengthPref)
    updateObj[`${path}/housingType`] = parseInt(data.housingType)
    // Facilities
    for (let key in data.facilities) {
      if (!data.facilities[key]) delete data.facilities[key]
    }
    updateObj[`${path}/facilities`] = data.facilities
    updateObj[`${path}/published`] = null
    console.log('AFTER EDIT', updateObj)

    // GeoFire
    if (data.address.lat && data.address.lng) {
      var geoFire = new GeoFire(db.ref(`${path}/address`));
      return geoFire.set({
        "geoFireLocation": [parseFloat(data.address.lat), parseFloat(data.address.lng)],
      })
      .then(function() {
        console.log("Provided keys have been added to GeoFire");
        return db.ref().update(updateObj).then(res => res)
      });
    }
    return db.ref().update(updateObj).then(res => res)
  }

  function publishListing(listingId, state) {
    if (!listingId) return
    const updateObj = {}
    const path = `listings/${listingId}`
    updateObj[`${path}/published`] = state
    console.log(updateObj)
    return db.ref().update(updateObj).then(res => res)
  }

  function createListing(input) {
    let data = Object.assign({}, input)
    // Sanitize
    const pushId = db.ref().push().key; // Get a new ID
    console.log(pushId)
    delete data.address.lat
    delete data.address.lng
    delete data.buildingName
    delete data.streetName
    delete data.postalCode
    delete data.lat
    delete data.lng

    // Basic
    data.address.postalCode = parseInt(data.address.postalCode)
    data.rent = parseInt(data.rent) * 100
    data.bedrooms = parseInt(data.bedrooms)
    data.bathrooms = parseInt(data.bathrooms)
    // Selects
    data.leaseType = parseInt(data.leaseType)
    data.leaseLengthPref = parseInt(data.leaseLengthPref)
    data.housingType = parseInt(data.housingType)

    let upd = {}
    upd[pushId] = true

    db.ref(`listingsByUser/${data.listedBy}`).update(upd)
    return db.ref('listings/' + pushId)
      .set(data)
      .then(res => pushId)
      .then(res => {
        if (data.address.lat && data.address.lng) {
          var geoFire = new GeoFire(db.ref(`listings/${pushId}/address`));
          return geoFire.set({
            "geoFireLocation": [parseFloat(data.address.lat), parseFloat(data.address.lng)],
          })
        }
      })
      .then(function() {
        console.log('What is the problem?', pushId)
        return pushId
      })


  }


  return listing
}
