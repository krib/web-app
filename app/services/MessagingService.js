import firebase from './firebase'
import {housingTypes, leaseTypes, leaseLengths} from './StaticModelsService'
import moment from 'moment'
import md5 from 'blueimp-md5'
import parallel from 'async/parallel';

const db = firebase.database()

export default () => {
  const messages = {
    createMessage,
    createConversation,
    getConversations,
    getConversation,
    readConversation
  }

  function getConversations(userId) {
    return db.ref('conversationsByUser/'+userId)
      .once('value')
      .then(res => res.val())
      .then(res => {
        // Conversation
        let promises = []
        for (let key in res) {
          let convPromise = db
            .ref('conversations/' + res[key])
            .once('value')
            .then(conv => conv.val())
            .then(conv => {
              conv.userId = key
              conv.key = res[key]
              return conv
            })
          promises.push(convPromise)
        }
        return Promise.all(promises)
      })
      .then(res => {
        // User
        let promises = []
        res.map((item) => {
          let userPromise = db
            .ref('usersPublic/' + item.userId)
            .once('value')
            .then(res => res.val())
            .then(res => {
              item.user = res
              return item
            })
          promises.push(userPromise)
        })
        return Promise.all(promises)
      })
  }

  function getConversation(id, userId) {
    return db
      .ref(`conversations/${id}`)
      .once('value')
      .then(res => res.val())
      .then(res => {
        for (let key in res.participants) {
          if (key !== userId) {
            return db
              .ref(`users/${key}`)
              .once('value')
              .then(sender => {
                res.sender = sender.val()
                return res
              })
          }
        }
      })
  }

  function createMessage(data) {
    if (!data.message || !data.userId || data.listedBy) return
    const pushId = db.ref().push().key; // Get a new ID
    const updateObj = {}
    updateObj[`conversationMessages/${data.conversationId}/${pushId}/senderId`] = data.userId
    updateObj[`conversationMessages/${data.conversationId}/${pushId}/text`] = data.message
    updateObj[`conversationMessages/${data.conversationId}/${pushId}/timestamp`] = moment().valueOf()
    updateObj[`conversations/${data.conversationId}/lastMessage`] = data.message
    return db.ref().update(updateObj)
  }

  function createConversation(data, convId) {
    if (!data.message || !data.userId || !data.listedBy) return
    const msgId = db.ref().push().key; // Get a new ID
    const updateObj = {}
    // Conversation
    updateObj[`conversations/${convId}/lastMessage`] = data.message
    updateObj[`conversations/${convId}/createdDate`] = moment().valueOf()
    updateObj[`conversations/${convId}/participants/${data.userId}`] = true
    updateObj[`conversations/${convId}/participants/${data.listedBy}`] = true
    // Send Message
    updateObj[`conversationMessages/${convId}/${msgId}/senderId`] = data.userId
    updateObj[`conversationMessages/${convId}/${msgId}/text`] = data.message
    updateObj[`conversationMessages/${convId}/${msgId}/timestamp`] = moment().valueOf()
    updateObj[`conversationMessages/${convId}/${msgId}/listingId`] = data.listingId
    // Convs By user
    // updateObj[`conversationsByUser/${data.userId}/${convId}`] = true
    // updateObj[`conversationsByUser/${data.listedBy}/${convId}`] = true

    console.log(updateObj)
    return db.ref().update(updateObj).then(res => convId)
  }

  function readConversation(convId, userId) {
    const updateObj = {}
    updateObj[`conversations/${convId}/participants/${userId}`] = 0
    return db.ref().update(updateObj).then(res => convId)
  }


  return messages
}
