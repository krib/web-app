import firebase from './firebase'

const db = firebase.database()
const storage = firebase.storage()

export default () => {
	const users = {
		getUser,
		updateUser,
		uploadPhoto,
		updateToken,
		getPhoto
	}

	function getUser(id) {
		return db.ref('users/' + id)
			.once('value')
			.then(res => res.val())
	}

	function updateUser(user) {
		//if (!user.displayName || !user.email || user.id) return false
		// Users Collection
		const updateObj = {}
		// updateObj[`users/${user.id}/id`] = user.id
		updateObj[`users/${user.id}/displayName`] = user.displayName
		updateObj[`users/${user.id}/email`] = user.email
		updateObj[`users/${user.id}/phoneNumber`] = user.phoneNumber
		// Public Collection
		updateObj[`usersPublic/${user.id}/displayName`] = user.displayName
		updateObj[`usersPublic/${user.id}/phoneNumber`] = user.phoneNumber
		return db.ref().update(updateObj)
	}

	function updateToken(userId, token) {
		const updateObj = {}
		updateObj[`users/${userId}/registrationToken`] = token
		return db.ref().update(updateObj)
	}

	function uploadPhoto(userId, photo) {
		return storage
			.ref(`usersPublic/${userId}/profilePhoto.jpg`)
			.put(photo)
			.then(res => res)
	}

	function getPhoto(userId) {
		const ref = db.ref(`usersPublic/${userId}/photoUrl`)
		return ref.once('value')
			.then(snap => {
				if (snap.exists()) {
					return snap.val()
				}

				return `/public/images/avatar_placeholder.png`
			})
	}

	return users
}
