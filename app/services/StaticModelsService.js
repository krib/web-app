export const housingTypes = [
  {name: 'HDB Apartment', key: 0, alias: 'hdbApartment'},
  {name: 'Condominium', key: 1, alias: 'condominium'},
  {name: 'Landed House', key: 2, alias: 'landedHouse'},
]

export const leaseTypes = [
  {name: 'Common Room', key: 0, alias: 'commonRoom'},
  {name: 'Master Room', key: 1, alias: 'masterRoom'},
  {name: 'Entire Place', key: 2, alias: 'entirePlace'},
]

export const leaseLengths = [
  {name: 'Short Term (3 - 6 months)', key: 0, alias: 'shortTerm'},
  {name: 'Long Term (6 - 36 months)', key: 1, alias: 'midTerm'},
  {name: 'Flexible', key: 2, alias: 'longTerm'},
]

export const otherPreferences = [
  {name: 'cooking', key: 0},
  {name: 'pets', key: 1},
]

export const facilities = {
  "Air-conditioning": false,
  "Utilities Included": false,
  "WiFi Included": false,
  "Air-conditioning": false,
  "King Bed": false,
  "Queen Bed": false,
  "Single Bed": false,
  "Sofa": false,
  "Dining Table": false,
  "Curtains": false,
  "Balcony": false,
  "Water Heater": false,
  "TV": false,
  "Refrigerator": false,
  "Stove": false,
  "Convection Oven": false,
  "Microwave Oven": false,
  "Washing Machine": false,
  "Dryer": false,
  "Pool": false,
  "Gym": false,
  "Tennis Courts": false,
  "Basketball Courts": false,
  "BBQ Pits": false,
  "Function Room": false,
}
