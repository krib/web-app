import firebase from './firebase'
import moment from 'moment'

const db = firebase.database()

export default () => {
  const listing = {
    updateCurationRequest,
    getCurationRequest
  }

  function getCurationRequest(userId) {
    console.log(userId)

    return db.ref('curateRequests/' + userId)
      .once('value')
      .then(res => res.val())
  }

  function updateCurationRequest(userId, data) {
    console.log(data)
    // Sanitization
    let update = Object.assign({}, data)
    delete update._INPUT
    return db.ref('curateRequests/' + userId).set(update).then(res => res)
  }


  return listing
}
