import firebase from './firebase'

const db = firebase.database()
const storage = firebase.storage()

export default () => {
  const photos = {
    uploadPhotos,
    deletePhoto
  }

  function uploadPhotos(listingId, files, minValue) {
    // Upload Photos
    console.log(listingId, files)

    let promises = []
    files.map(file => {
      minValue++;
      let promise = storage
        .ref(`listings/${listingId}/photos/photo${minValue}.jpg`)
        .put(file)
        .then(res => {
          // Add to database
          const updateObj = {}
          return db.ref().update(updateObj)
        })

        promises.push(promise)
    })
    return Promise.all(promises)
  }

  function deletePhoto(listingId, key) {
    return storage
      .ref(`listings/${listingId}/photos/${key}.jpg`)
      .delete()
      .then(res => {
        const updateObj = {}
        updateObj[`listings/${listingId}/photos/${key}`] = null
        return db.ref().update(updateObj)
      })

  }

  return photos
}
