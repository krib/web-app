import config from '../firebase.config.js'
import firebase from 'firebase'


if (firebase.apps.length === 0) {
    firebase.initializeApp(config)
}

export default firebase
