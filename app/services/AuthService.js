import firebase from './firebase'

const db = firebase.database()

export default () => {
  const auth = {
    Authenticate,
  }

  function Authenticate(phoneNumber) {
    return firebase.auth()
      .signInWithPhoneNumber(phoneNumber, recaptchaVerifier)
      .then(res => res)
      .catch(err => console.log(err))
  }


  return auth
}
