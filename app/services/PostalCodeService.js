import firebase from './firebase'
import moment from 'moment'
import { CLOUD_FUNCTIONS_HOSTNAME } from '../app.config'

export default () => {
	const pk = {
		getPostalCodeData
	}

	function getPostalCodeData(postalCode) {
		const url = `${CLOUD_FUNCTIONS_HOSTNAME}/user_api/locations?postalCode=${postalCode}`
		return fetch(url)
			.then(res => res.json())
			.then(res => res)
	}
	return pk
}
