import firebase from './firebase'
import { LANDLORD, AREAS, HOUSING, LEASE, CURATED } from '../constants/listingConstants'
import { housingTypes, leaseTypes, leaseLengths } from './StaticModelsService'
import { LISTING_CONFIG, MESSAGING_CONFIG } from '../app.config.js'

const db = firebase.database()


export default () => {
	const listings = {
		getListings,
		getListingsDetailed,
		getListing,
		getLandlords,
		getMessages,
		getUserConversations,
		listenMessages,
		unlistenMessages,
		listenPhotos,
		unlistenPhotos
	}

	function getListingsDetailed(target, owner, start, perPage) {
		if (!perPage) perPage = 1000;
		let link = 'listingsByUser/' + owner
		let obj = {}
		if (start) obj = db.ref(link).startAt(start)
		if (!start) obj = db.ref(link)
		return obj
			.orderByKey()
			.limitToFirst(start ? perPage + 1 : perPage)
			.once('value')
			.then(snapshots => mapSnapshots(snapshots))
			.then(listings => mapPhotos(listings))
			.then(listings => mapLandlord(listings))
			.then(listings => mapStatic(listings))
			.then(listings => {
				if (start) listings.shift()
				return listings
			})
	}


	function getListings(target, params, start, perPage) {
		let link = ''

		switch (target) {
			case LANDLORD:
				link = 'listingsByUser/' + params
				break;
			case AREAS:
				link = 'activeListings/byArea/' + params
				break;
			case HOUSING:
				link = 'activeListings/byHousingType/' + params
				break;
			case LEASE:
				link = 'activeListings/byLeaseType/' + params
				break;
			case CURATED:
				link = 'curatedListings/' + params
				break;
			default:
				link = 'activeListings/all'
		}

		if (perPage === null || perPage === undefined) perPage = LISTING_CONFIG.perPage;
		if (start === null || start === undefined) start = -4656799881000

		return db.ref(link)
			.orderByValue()
			.startAt(start)
			.limitToFirst(perPage)
			.once('value')
			.then(snapshots => mapSnapshots(snapshots))
			.then(listings => mapPhotos(listings))
			// .then(listings => mapLandlord(listings))
			.then(listings => mapStatic(listings))
			.then(listings => {
				return listings
			})
	}

	function listenPhotos(id, cb) {
		return db.ref('listings/' + id + '/photos')
			.on('value', res => {
				cb(res)
				console.log('CHANGED', id)
			})
	}

	function unlistenPhotos(id) {
		db.ref('listings/' + id + '/photos').off()
	}

	function listenMessages(id, cb) {
		return db.ref('conversationMessages/' + id)
			.orderByKey()
			.limitToLast(MESSAGING_CONFIG.perPage)
			.on('value', res => {
				res = res.val()
				let items = []
				for (let item in res) {
					items.push({ key: item, ...res[item] })
				}
				mapProp(items, 'senderId', 'usersPublic', 'sender').then(res => cb(res))
			})
	}

	function unlistenMessages(id) {
		db.ref('conversationMessages/' + id).off()
	}

	function getMessages(id, start, limit) {
		console.log(id, start, limit)
		let obj = {}
		if (!limit) limit = MESSAGING_CONFIG.perPage
		if (start) obj = db.ref('conversationMessages/' + id).endAt(start)
		if (!start) obj = db.ref('conversationMessages/' + id)
		return obj
			.orderByKey()
			.limitToLast(limit)
			.once('value')
			.then(res => res.val())
			.then(res => {
				let items = []
				for (let item in res) {
					items.push({ key: item, ...res[item] })
				}
				if (start) items.pop()
				return items
			})
			.then(res => mapProp(res, 'senderId', 'usersPublic', 'sender'))
			.then(res => res)
	}

	function getUserConversations(userId) {
		return db.ref('conversationsByUser/' + userId)
			.once('value')
			.then(res => res.val())
			.then(res => objectToArray(res))
			.then(res => callArray(res, 'conversations'))
			.then(res => callProp(res, 'participants', 'usersPublic'))
			.then(res => res)
	}

	function getLandlords() {
		console.log("GET LANDLORDS")
		return db.ref('listingsByUser')
			.once('value')
			.then(landlords => landlords.val())
			.then(landlords => {
				let data = []
				for (let landlord in landlords) {
					// KEYS
					let listings = []
					for (let listing in landlords[landlord]) {
						listings.push(listing)
					}
					let listedBy = landlord
					data.push({ listings, listedBy })
				}
				return data
			})
			.then(data => mapLandlord(data))
			.then(data => data)
	}

	function getListing(key) {
		return db.ref(`listings/${key}`)
			.once('value')
			.then(listing => [{ data: listing, key }])
			.then(listings => mapPhotos(listings))
			.then(listings => mapLandlord(listings))
			.then(listings => mapStatic(listings))
			.then(listings => {
				return listings[0]
			})
	}


	function mapStatic(listings) {
		return listings.map((listing) => {
			// Finish touch
			listing.housingType = housingTypes.find((item) => item.key === listing.housingType)
			listing.leaseType = leaseTypes.find((item) => item.key === listing.leaseType)
			listing.leaseLengthPref = leaseLengths.find((item) => item.key === listing.leaseLengthPref)
			// Fix
			if (!listing.housingType) listing.housingType = {}
			if (!listing.leaseType) listing.leaseType = {}
			if (!listing.leaseLengthPref) listing.leaseLengthPref = {}

			return listing
		})
	}

	function mapSnapshots(snapshots) {
		let listings = []
		snapshots.forEach(snapshot => {
			listings.push(snapshot.key)
		})
		
		let promises = []
		listings.map((key) => {
			let promise = db
				.ref(`/listings/${key}`)
				.once('value')
				.then(listing => ({ data: listing, key }))
			promises.push(promise)
		})

		return Promise.all(promises)
	}

	function callProp(arr, propName, reference) {
		let promises = []
		arr.map((item) => {
			item[propName] = objectToArray(item[propName])
			let promise = callArray(item[propName], reference)
				.then(res => {
					item[propName] = res;
					return item
				})
			promises.push(promise)
		})
		return Promise.all(promises)
	}

	function mapProp(arr, prop, reference, newName) {
		let promises = []
		arr.map((item) => {
			let promise = db
				.ref(reference + '/' + item[prop])
				.once('value')
				.then(res => {
					let key = item[prop]
					item[newName || prop] = res.val()
					if (item[newName || prop]) item[newName || prop].key = key
					return item
				})
			promises.push(promise)
		})
		return Promise.all(promises)
	}

	function objectToArray(obj) {
		let arr = []
		for (let prop in obj) {
			arr.push(prop)
		}
		return arr
	}

	function callArray(arr, reference) {
		let promises = []
		arr.map((key) => {
			let promise = db.ref(reference + '/' + key).once('value').then(res => {
				let obj = {}
				if (res.val()) obj = res.val()
				obj.key = key

				return obj
			})
			promises.push(promise)
		})
		return Promise.all(promises)
	}

	function mapPhotos(listings) {
		let promises = []
		listings.map((listing) => {
			let key = listing.key
			listing = listing.data.val()
			if (!listing) listing = {}
			listing.key = key;
			// PHOTOS
			let photos = getPhoto(listing)
			promises.push(photos)
		})
		
		return Promise.all(promises)
	}

	function mapLandlord(listings) {
		let promises = []
		listings.map((listing) => {
			let promise = db
				.ref(`usersPublic/${listing.listedBy}`)
				.once('value')
				.then(res => {
					listing.landlord = res.val()
					return listing
				})
			promises.push(promise)
		})
		return Promise.all(promises)
	}

	// Returns Listing with photo
	function getPhoto(listing) {
		const photos = []
		for (let photo in listing.photos) {
			if (photo.indexOf('thumb_') === -1) {
				let obj = {
					key: photo
				}
				obj.url = listing.photos[photo]
				obj.thumb_url = listing.photos[`thumb_${photo}`]
				photos.push(obj)
			}

		}
		listing.photos = photos;
		return listing
	}

	return listings
}
