import firebase from './firebase'

const db = firebase.database()


export default () => {
	const listings = {
		getCounts
	}

	// function getCounts() {
	// 	return db.ref('activeListingsCount')
	// 		.once('value')
	// 		.then(res => res.val())
	// 		.then(res => {
	// 			let arr = {
	// 				byArea: [],
	// 				byHousingType: res.byHousingType,
	// 				byLeaseType: res.byLeaseType
	// 			}
	// 			for (let key in res.byArea) { arr.byArea.push({ name: key, count: res.byArea[key] }) }
	// 			return arr
	// 		})
	// }

	function getCounts() {
		return db.ref('activeListingsCount/byArea').orderByValue()
			.once('value')
			.then(snaps => {
				let byArea = []

				snaps.forEach(snap => {
					const name = snap.key
					const count = snap.val()
					byArea.push({ name: name, count: count })
				})

				let arr = {
					byArea: byArea.reverse()
				}

				return arr
			})
	}

	return listings
}

export const areas = [
  "Ang Mo Kio",
  "Bedok",
  "Bishan",
  "Bukit Batok",
  "Bukit Merah",
  "Bukit Panjang",
  "Bukit Timah",
  "Buona Vista",
  "Central Business District",
  "Changi",
  "Choa Chu Kang",
  "Clementi",
  "Geylang",
  "Holland",
  "Hougang",
  "Jurong East",
  "Jurong Island",
  "Jurong West",
  "Kallang",
  "Katong",
  "Kranji",
  "Lim Chu Kang",
  "Little India",
  "Marine Parade",
  "Novena",
  "Orchard",
  "Outram",
  "Pasir Ris",
  "Paya Lebar",
  "Pulau Tekong",
  "Pulau Ubin",
  "Punggol",
  "Queenstown",
  "Seletar",
  "Sembawang",
  "Sengkang",
  "Sentosa",
  "Serangoon",
  "Tampines",
  "Telok Blangah",
  "Toa Payoh",
  "Tuas",
  "Woodlands",
  "Yio Chu Kang",
  "Yishun",
]