import {REQUEST_USER, RECEIVE_USER, REJECT_USER, SET_FIREBASE_USER, RECEIVE_USER_PHOTO} from '../actions/userActions'

export default (state={}, action) => {
  switch (action.type) {
    case SET_FIREBASE_USER:
      return {...state, auth: action.user}
    break;
    case REQUEST_USER:
      return {...state, id: action.userId}
    break;
    case RECEIVE_USER:
      return {...state, ...action.user, isFetched: true}
    break;
    case RECEIVE_USER_PHOTO:
      return {...state, photoUrl: action.photo}
    break;
    case REJECT_USER:
      return {isFetched:true}
    break;
    default:
      return state
  }
}
