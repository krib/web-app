const SHOW_LOGIN_FORM = 'SHOW_LOGIN_FORM'
export const showLoginForm = () => ({
  type: SHOW_LOGIN_FORM
})

const HIDE_LOGIN_FORM = 'HIDE_LOGIN_FORM'
export const hideLoginForm = () => ({
  type: HIDE_LOGIN_FORM
})

export default (state = false, action) => {
  switch (action.type) {
    case SHOW_LOGIN_FORM:
      return true
    break;
    case HIDE_LOGIN_FORM:
      return false
    break;
    default:
      return state
  }
}
