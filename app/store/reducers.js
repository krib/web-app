import { combineReducers } from 'redux'

import user from '../reducers/userReducer'
import loginReducer from '../reducers/loginReducer'


export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    user,
    loginForm: loginReducer
  })
}

export default makeRootReducer
