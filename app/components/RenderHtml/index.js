import React from 'react'
const reader = new FileReader();

export default class extends React.Component {
  state = {
    html: {__html: ''}
  }

  componentDidMount() {
    fetch('/public' + this.props.url + '.html')
    .then(res => res.blob())
    .then(html => {
      reader.readAsText(html)
      reader.onload = () => {
          this.setState({html: {__html: reader.result}})
      }
    })
  }

  render() {
    return (
      <div dangerouslySetInnerHTML={this.state.html} />
    )
  }
}
