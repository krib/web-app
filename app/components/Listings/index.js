import React from 'react'
import ListingItem from '../ListingItem'
import {Link} from 'react-router-dom'

const Listings = ({ listings, loadMore, noResults, isFetching }) => (
  <div>
    <div className="row">
      {
        listings.map((listing, idx) =>
            <ListingItem key={idx} listing={listing} />
        )
      }
    </div>

    {
      !listings.length && !isFetching &&
      <div>
        <h5>Oops, looks like we don't have any property listings here!</h5>
        <p>Couldn't find a suitable place? Tell us what you are looking for and we will search daily through thousands of listings on multiple sites to find the best match for you.</p>

        <Link  to="/settings" className="btn btn-info">Try PropertyMatch</Link>
        </div>
    }

    <div className="text-right">
    {
      listings.length > 0 && !noResults &&
      <button className="btn btn-primary btn-sm" onClick={loadMore}>Load More</button>
    }
    {
      listings.length > 0  && noResults &&
      <button className="btn btn-primary btn-sm" disabled>No More Results</button>
    }


    </div>
  </div>
)

export default Listings
