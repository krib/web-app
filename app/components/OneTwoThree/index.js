import React from 'react'

export default class extends React.Component {
	render() {
		const boxStyle = {
			'textAlign': 'center',
			'width': '100%'
		}

		const imageStyle = {
			'width': '48px',
			'height': '48px'
		}

		const containerFluidStyle = {
			'paddingTop': '5%',
			'backgroundColor': 'white'
		}

		const headerStyle = {
			'display': 'block',
			'height': '3rem'
		}

		return (
			<div className="container-fluid" style={containerFluidStyle}>
				<div className="row">
					<div className="col-sm-4" style={boxStyle}>
						<h5 className="header" style={headerStyle}>{this.props.items[0].title}</h5>
						<div className="card-block">
							<div className="image-container">
								<img className="image" src={this.props.items[0].image} style={imageStyle} />
							</div>
							<br/>
							<p className="card-text">{this.props.items[0].description}</p>
						</div>
					</div>
					<div className="col-sm-4" style={boxStyle}>
						<h5 className="header" style={headerStyle}>{this.props.items[1].title}</h5>
						<div className="card-block">
							<div className="image-container">
								<img className="image" src={this.props.items[1].image} style={imageStyle} />
							</div>
							<br/>
							<p className="card-text">{this.props.items[1].description}</p>
						</div>
					</div>
					<div className="col-sm-4" style={boxStyle}>
						<h5 className="header" style={headerStyle}>{this.props.items[2].title}</h5>
						<div className="card-block">
							<div className="image-container">
								<img className="image" src={this.props.items[2].image} style={imageStyle} />
							</div>
							<br/>
							<p className="card-text">{this.props.items[2].description}</p>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
