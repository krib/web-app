import React from 'react'
import AppLayout from './../../layouts/AppLayout'
export default () => (
  <AppLayout>
    <div className="please-wait">
      <div>
        <img src="/public/logo-icon.svg" className="logo"/> <br/>
        <strong>Please Wait</strong>
      </div>
    </div>
  </AppLayout>

)
