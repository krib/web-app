import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import queryString from 'query-string'
import store from 'store'

// Service
import AuthService from './../../services/AuthService'
import firebase from './../../services/firebase'

// Recaptcha
delete window.recaptchaVerifier;
window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha', {
  size: 'invisible',
});


class SignInModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      phone: '+65',
      confirmation: ''
    }
  }

  componentDidMount() {
    let location = queryString.parse(window.location.search);
    if (location.source && location.source !== 'undefined') {
      console.log('SIGNIN SOURCE SET', location.source)
      store.set('source', location.source)
    }
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    recaptchaVerifier
      .verify()
      .then(res=> {
        AuthService()
          .Authenticate(this.state.phone)
          .then(confirmationResult => {
            if (!confirmationResult) {
              Promise.reject()
              this.setState({isError: true})
              return
            }
            setTimeout(()=>{
              console.log('RESPONSE FROM', confirmationResult)
              this.setState({confirmationResult, isError: false})

            }, 500)
          })
      })
  }
  onConfirm = (evt) => {
    evt.preventDefault()
    this.state.confirmationResult
      .confirm(this.state.confirmation)
      .then(res=>{
        this.setState({isError: false})
        this.props.hide()
      })
      .catch(err => this.setState({isError: true}))
  }

  handleChange = (evt) => {
    let obj = {}
    obj[evt.target.name] = evt.target.value
    this.setState(obj)
  }

  render = () => {
    // if (this.props.user.displayName || this.state.redirect) return <Redirect to={`/`} />
    if (this.props.user.registrationToken || this.state.redirect) return <Redirect to={`/profile`} />
    return (
      <div className="modal" style={{ display: 'block' }}>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Sign In To Krib</h5>
              <button type="button" className="close" onClick={ this.props.hide }>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {
                this.state.isError &&
                <div className="alert alert-danger">
                  <strong>Error!</strong> <br/>
                  Sign In Failed. Please try again.
                </div>
              }

              {
                !this.state.confirmationResult
                ?
                  <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                      <label>Simply log in with your phone number. A verification code will be sent to you via SMS.</label>
                      <input type="text" value={this.state.phone} onChange={this.handleChange} name="phone" className="form-control" />
                      <div className="spacer-sm"></div>
                    </div>
                    <div className="form-group text-right">
                      <input type="submit" value="Sign In" className="btn btn-lg btn-primary btn-block" />
                    </div>
                  </form>
                :
                  <form onSubmit={this.onConfirm}>
                    <div className="form-group">
                      <label>Enter the verification code.</label>
                      <input type="text" value={this.state.confirmation} onChange={this.handleChange} name="confirmation" className="form-control" />
                      <p>Did not receive a verification code? Click <a href="#" onClick={()=>this.setState({confirmationResult: false})}>here</a> to try again.</p>
                    </div>
                    <div className="form-group text-right">
                      <input type="submit" value="Sign In" className="btn btn-lg btn-primary btn-block" />
                    </div>
                  </form>
              }
              <div className="text-center">
        				<p>By signing in you agree to our Terms of Use.</p>
                <Link to="pages/terms" className="btn btn-link">Terms of Use</Link>
                <span className="text-muted"> | </span>
                <Link to="pages/privacy" className="btn btn-link">Privacy Policy</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})
export default connect(mapStateToProps)(SignInModal)
