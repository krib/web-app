import React from 'react'
import { Link } from 'react-router-dom'

export default class extends React.Component {
	render = () => (
		<footer>
			<div className="container">
				<div className="row">
					<div className="col-md-3">
						<img src="/public/logo.svg" alt="" className="logo" /> <br />
						<small className="text-muted">
							All rights reserved © 2017 <br />
							Krib Online Pte Ltd
           			 	</small> <br />
						<a href="https://www.facebook.com/teamkrib" className="fa fa-facebook" target="_blank"></a>
						&nbsp;&nbsp;&nbsp;
						<a href="https://twitter.com/teamkrib" className="fa fa-twitter" target="_blank"></a>
					</div>
					<div className="col-md-3">
						<div className="spacer hidden-md-up"></div>
						<h6 className="text-primary">Company</h6>
						<div>
							<a href="/pages/about">About</a>
							<a href="/pages/contact">Contact</a>
							<a href="terms">Terms of Use</a>
							<a href="/pages/privacy">Privacy</a>
						</div>
					</div>
					<div className="col-md-3">
						<div className="spacer hidden-md-up"></div>
						<h6 className="text-primary">Landlords / Agents</h6>
						<a href="/pages/faq">Why list with us?</a> {/*TODO: This should be unique content*/}
						<a href="/pages/landlord-guide-to-renting">Landlord Guide</a>
						<a href="/new-listing">​List Property</a>
					</div>
					<div className="col-md-3">
						<div className="spacer hidden-md-up"></div>
						<h6 className="text-primary">Tenants</h6>
						<a href="/pages/faq">Why use Krib?</a> {/*TODO: This should be unique content*/}
						<a href="/pages/tenant-guide-to-renting">Tenant Guide</a>
						<a href="/settings">PropertyMatch</a>
					</div>

				</div>

			</div>
		</footer>

	)
}
