import React from 'react'
import { NavLink, Link, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { rejectUser } from './../../actions/userActions'
import firebase from './../../services/firebase'
import SignInModal from '../SignInModal'
import { showLoginForm, hideLoginForm } from './../../reducers/loginReducer'


class Header extends React.Component {

	constructor() {
		super()
		this.state = {}
	}

	signOut = () => {
		this.props.signOutUser()
		firebase.auth().signOut()
			.then(res => {
				console.log("Signing Out")
				this.setState({ redirect: true })
			})
	}
	render = () => {
		if (this.state.redirect) return <Redirect to="/" />
		if (this.state.redirectToNewListing) return <Redirect to="/new-listing" />
		return (
			<div>
				<nav className="navbar navbar-toggleable-md navbar-light bg-faded  fixed-top">

					<button className="navbar-toggler navbar-toggler-right" onClick={() => this.setState({ toggleMenu: !this.state.toggleMenu })}>
						<span className="navbar-toggler-icon"></span>
					</button>
					<a className="navbar-brand" href="/"><img src="/public/logo.svg" alt="Krib" style={{ transform: 'translateY(-2px)' }} /></a>
					<div className=" navbar-collapse" id="navbarSupportedContent" style={{ display: this.state.toggleMenu ? 'block' : 'none' }}>
						<ul className="navbar-nav mr-auto">
							<li className="nav-item">
								<a href="/" className="nav-link">Home</a>
							</li>
							{/* <li className="nav-item">
  							<a href="/" className="nav-link">Blog</a>
  						</li> */}
							<li className="nav-item">
								<a href="/pages/about" className="nav-link">About</a>
							</li>
							<li className="nav-item">
								<a href="https://blog.krib.co/" className="nav-link" target="_blank">Blog</a>
							</li>
							<li className="nav-item">
								<a href="/pages/contact" className="nav-link">Contact</a>
							</li>
						</ul>

						<ul className="navbar-nav mr-right">
							{
								!this.props.user.registrationToken &&
								<li className="nav-item">
									<NavLink to="/settings" className="nav-link">PropertyMatch</NavLink>
								</li>
							}
							{
								!this.props.user.registrationToken &&
								<li className="nav-item">
									<NavLink to="/new-listing" className="nav-link" >List Property</NavLink>
								</li>
							}
							{
								!this.props.user.registrationToken &&
								<li className="nav-item">
									<a className="nav-link" onClick={this.props.showLoginForm}>Log In</a>
									{/* <Link to="/signin" className="nav-link">Log In</Link> */}
								</li>
							}
							{
								this.props.user.registrationToken &&
								<li className="nav-item">
									<NavLink to="/curated-listings" className="nav-link" >PropertyMatch</NavLink>
								</li>
							}
							{
								this.props.user.registrationToken &&
								<li className="nav-item">
									<NavLink to="/my-listings" className="nav-link">My Properties</NavLink>
								</li>
							}
							{
								this.props.user.registrationToken &&
								<li className="nav-item">
									<NavLink to="/messages" className="nav-link">Messages</NavLink>
								</li>
							}
							{
								this.props.user.registrationToken && this.state.toggleMenu &&
								<li className="nav-item">
									<Link to="/profile" className="nav-link">My Profile</Link>
								</li>
							}

							{
								this.props.user.registrationToken && this.state.toggleMenu &&
								<li className="nav-item">
									<li><a href="#" onClick={this.signOut}>Sign Out</a></li>
								</li>
							}
							{
								this.props.user.id &&
								<li className="nav-item">
									{
										!this.state.toggleMenu &&
										<div className="avatar" onClick={() => this.setState({ dropdown: !this.state.dropdown })}
											style={{ backgroundImage: `url(${this.props.user.photoUrl})` }}
										></div>
									}

									{
										this.state.dropdown &&
										<ul className="avatar-dropdown">
											<li><Link to="/profile">My Profile</Link></li>
											<li><a href="#" onClick={this.signOut}>Sign Out</a></li>
										</ul>
									}

								</li>
							}

						</ul>

					</div>
				</nav>
				{
					this.props.loginForm &&
					<SignInModal hide={this.props.hideLoginForm} />
				}

			</div>
		)
	}
}
const mapStateToProps = state => ({
	user: state.user,
	loginForm: state.loginForm
})

const mapDispatchToProps = dispatch => ({
	signOutUser: () => dispatch(rejectUser()),
	showLoginForm: () => dispatch(showLoginForm()),
	hideLoginForm: () => dispatch(hideLoginForm()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
