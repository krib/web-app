import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.scss'
import {connect} from 'react-redux'


const ListingItem =  ({ listing, user }) => (

    <div className="col-lg-3 col-md-4 col-sm-6 col-xs-12">
      <Link to={`/listing/${listing.key}`}>
        <div className="listing-photo" style={{backgroundImage: `url(${listing.photos.length ? listing.photos[0].thumb_url: 'inherit'})`}}></div>
      <p>
        <span className="text-secondary text-bold">
          ${ (Math.round(listing.rent / 100)).toLocaleString() + ' ' }</span>
          {listing.leaseType.name}
          { listing. area ? ' in ' + listing.area : ''}
          <br/>
        <span className="text-muted">{listing.housingType.name}</span>
        {' '}
          {
            listing.leaseType.key === 2 &&
            <span className="text-muted">
              (
              <i className="fa fa-fw fa-bed"></i> {listing.bedrooms} {' '}
              <i className="fa fa-fw fa-bath"></i> {listing.bathrooms}
              )
            </span>
          }

        <br/>


        <span className="text-muted">{listing.leaseLengthPref.name}</span> <br/>

        {
          listing.listedBy === user.id &&
          <span>
            <Link to={`/edit-listing/${listing.key}`}>
              Edit
            </Link>
          </span>
        }
      </p>
      <div className="spacer-lg"></div>
      </Link>
    </div>

)

const mapStateToProps = state => ({
  user: state.user
})

export default connect(mapStateToProps)(ListingItem)
