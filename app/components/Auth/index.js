import React from 'react'
import {Redirect} from 'react-router-dom'
import {connect} from 'react-redux'

import firebase from './../../services/firebase'
import {fetchUser, setFirebaseUser, rejectUser} from './../../actions/userActions'
import UsersService from './../../services/UsersService'

class AuthWrapper extends React.Component {

  updateUserToken(userId, token) {
    UsersService()
      .updateToken(userId, token)
  }

  componentDidMount() {
    firebase
      .auth()
      .onAuthStateChanged(user => {
      if (user) {
        console.log('User is logged in')
        this.props.setFirebaseUser(user)
        this.props.fetchUser(user.uid)
        this.updateUserToken(user.uid, user.ie)
      } else {
        console.log('User is not logged in')
        // No user is signed in.
        this.props.rejectUser()
      }
    })
  }

  render = () => {
    return null
  }

}

const mapStateToProps = (state) => ({
  user: state.user
})

const mapDispatchToProps = (dispatch) => ({
  setFirebaseUser: user => dispatch(setFirebaseUser(user)),
  fetchUser: (userId) => dispatch(fetchUser(userId)),
  rejectUser: () => dispatch(rejectUser())
})

export default connect(mapStateToProps, mapDispatchToProps)(AuthWrapper)
