import UsersService from '../services/UsersService'

export const REQUEST_USER = 'REQUEST_USER'
const requestUser = (userId) => ({
  type: REQUEST_USER,
  userId
})

export const RECEIVE_USER = 'RECEIVE_USER'
const receiveUser = user => ({
  type: RECEIVE_USER,
  user
})

export const REJECT_USER = 'REJECT_USER'
export const rejectUser = user => ({
  type: REJECT_USER,
  user
})


export const SET_FIREBASE_USER = 'SET_FIREBASE_USER'
export const setFirebaseUser = user => ({
  type: SET_FIREBASE_USER,
  user
})

export const RECEIVE_USER_PHOTO = 'RECEIVE_USER_PHOTO'
const receiveUserPhoto = photo => ({
  type: RECEIVE_USER_PHOTO,
  photo
})

export const fetchUserPhoto = userId => dispatch => {
  UsersService()
    .getPhoto(userId)
    .then(photo => {
      dispatch(receiveUserPhoto(photo))
    })
}


export const fetchUser = userId => dispatch => {
  dispatch(requestUser(userId))
  UsersService()
    .getUser(userId)
    .then(user => {
      if (!user) {
        dispatch(rejectUser())
        return
      }
      dispatch(receiveUser(user))
      dispatch(fetchUserPhoto(userId))
      return user
    })


}
