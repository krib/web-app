// Constants for Listing Page

export const LANDLORD       = 'landlord'
export const AREAS          = 'area'
export const HOUSING        = 'housing'
export const LEASE          = 'lease'
export const CURATED        = 'curated'

// Housing
export const HDB_APARTMENT        = 'hdbApartment'
export const CONDOMINIUM          = 'condominium'
export const LANDED_HOUSE         = 'landedHouse'

// Lease types
export const COMMON_ROOM = 'commonRoom'
export const ENTIRE_PLACE = 'entirePlace'
export const MASTER_ROOM = 'masterRoom'
