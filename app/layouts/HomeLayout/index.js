import React from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'

import Header from './../../components/Header'
import Footer from './../../components/Footer'

export default ({ children  }) => (
  <div style={{marginTop: '55px'}}>
    <Header/>
    {children}
    <Footer />
  </div>
)
