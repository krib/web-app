import React from 'react'
import { Link } from 'react-router'
import PropTypes from 'prop-types'

import Header from './../../components/Header'
import Footer from './../../components/Footer'

export default ({ children  }) => (
  <div >
    <Header/>
      <div className="container" style={{marginTop: '6rem', marginBottom: '3rem'}}>
        {children}
      </div>
      <Footer />
  </div>
)
